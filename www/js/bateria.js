function onBatteryStatus(status) {
    storage.setItem('bateria',status.level);
    console.log("Level: " + status.level + " isPlugged: " + status.isPlugged);
    
    var icone = "battery_full";
	var cor = "green";

    if (status.level < 50) {
    	var cor = "yellow";
    } 
    if (status.level < 20) {
    	var cor = "red";
    }
    if (status.isPlugged) {
    	var icone = "battery_charging_full";
    }

    $('#batt').css("color", cor);
    $('#batt').html(icone);

}


// atualização
var onUpdt = function(data) {

 function sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
    }

    async function limpa() {
        await sleep(60000);
        deleteCaniApk();
    }
    limpa();

};

function onErrorUpdate(error) {
    alert('Houve um erro: ' + error.message);
    console.log(error);
}

$('#update').click(function () {
	window.cordova.plugins.FileOpener.openFile("http://canicontrol.vetorial.net/app/CaniMobile.apk", onUpdt, onErrorUpdate);
})


