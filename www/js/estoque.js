// $('#fechaEstoque').click(function (event) {
//     event.preventDefault();
//     $('#modalEstoque').modal('close');
//     event.stopPropagation();
// }); 

// $('#enviaEstoqueOk').click(function(event) {
//     event.preventDefault();
//     $('#enviaEstoqueOk').prepend('<i id="load" class="fa fa-spinner fa-spin left"></i>');
//     enviar('1');
// }); 

// $('#enviaEstoqueWar').click(function(event) {
//     event.preventDefault();
//     $('#enviaEstoqueWar').prepend('<i id="load" class="fa fa-spinner fa-spin left"></i>');
//     enviar('2');
// }); 

// function enviar(valor) {
//     if ( $('#selecCode').val() == "0" ) {
//         Materialize.toast("Favor preencher Estoque + Resposta.",4000);
//         $('#load').remove();
//         return;
//     }

//     var dadosAdicionais = "warn="+valor+"&aGiD="+clienteAGID+"&eqpid="+equipeID+"&";
//     var urlestoque = 'http://canicontrol.vetorial.net/class/InfraDao.php?'+dadosAdicionais+$('form').serialize();

//     $.ajax({
//         type: "GET",
//         url: urlestoque,
//         datatype: "html",
//         timeout: 10000,

//         error: function() {
//             voltaAtualiza();

//             clienteAtual = JSON.parse(storage.getItem(clienteAtual.agendaid));
            
//             clienteAtual.fim = horaAtual();
//             clienteAtual.agendastatus = "Concluído.";
//             clienteAtual.pendente = "pendenteEnvio";
//             clienteAtual.urlestoque = urlestoque;

//             storage.setItem(clienteAtual.agendaid, JSON.stringify(clienteAtual));

//             // insere objeto no banco
//             atualizabancoHist(clienteAtual);
//             atualizaListaOsUnica(clienteAtual.id);
//             console.log("ID no ESTOQUEJS "+ clienteAtual.id);
//         },
//         success: function(dados) {
//             voltaAtualiza();

//             clienteAtual = JSON.parse(storage.getItem(clienteAtual.agendaid));

//             clienteAtual.fim = horaAtual();
//             clienteAtual.agendastatus = "Concluído.";
//             clienteAtual.urlestoque = urlestoque;

//             storage.setItem(clienteAtual.agendaid, JSON.stringify(clienteAtual));

//             // insere objeto no banco
//             atualizabancoHist(clienteAtual);

//             atualizaLista();
//         }
//     });
// }

function voltaAtualiza() {
    $('#rOk').addClass('hide');
    $('#bntFormCorp').addClass('hide');
    $("#os").removeClass("hide");
    $('#btnMove').removeClass("hide");
    $('#atualizaOs').removeClass('hide');
    $("input").val('');
    $("textarea").val('');
    $('#selecCode').val(0);
    $('#selecCode').material_select();
    $("#materiais").val(0);
    $('#materiais').material_select();
    $("#obsFim").val('');
    $('#pause').addClass('hide');
    $('#load').remove();
    $("#enviaOk").prop('disabled', true);
    $("#enviaWar").prop('disabled', true);
    $("#enviaInv").prop('disabled', false);
    $('#load').remove();
    $('.tab a[href="#os"]').click();
    $('#sinc').prop('disabled', false);
    $('#informacao').html('');
    window.scrollTo(0, 0);
}


//limpa campos da aba estoque

$("#saveEstoque").click(function(e){
    e.preventDefault();
    salvarEstoque();    
});

function salvarEstoque() {
    
    if ( $("#saveEstoque").prop('disabled') == true ) {
        $("#saveEstoque").prop('disabled', false);
    }
    if ( $("#sendEstoque").prop('disabled') == true ) {
        $("#sendEstoque").prop('disabled', false);
    }
    
    estoque.resposta = [];
    var minutes = moment(new Date()).format("HH:mm:ss");
    
    $(".divEstoque").each(function(){
        var est = {
            nome: $(this).children('p').attr('data-valor'),
            texto: $(this).children('p').text(),
            valor: $(this).children('input').val()
        }
        estoque.resposta.push(est);
    });

    $("#tempo").html("Salvo às "+minutes);
    Materialize.toast('Salvo às '+minutes, 3000);
}

$(document).ready(function(){
    $('.modal').modal();
});


function renovaEstoque(param) {
    estoque = {};
    estoque.eqpid = credenciais.eqpid;
    estoque.resposta = [];
    estoque.cliente = '';
    estoque.circ = '';
    estoque.funcao = 'cadastra';
    estoque.tipo = {
        consumo: 'novo',
        retorno: 'novo'
    };

    if ( $("#saveEstoque").prop('disabled') == false ) {
        $("#saveEstoque").prop('disabled', true);
    }
    if ( $("#sendEstoque").prop('disabled') == false ) {
        $("#sendEstoque").prop('disabled', true);
    }
    $("#registro").html('');
    $("#obsFim").text('');
    $("#tempo").html('');
}

$("#addList").click(function(){

    if ( typeof(clienteAtual.agendaid) == 'undefined') {
        Materialize.toast('Necessário selecionar um cliente', 2000);
        return;
    } else {
        estoque.cliente = clienteAtual.agendaid;
        estoque.circ = clienteAtual.agendacircuito;
    }

    if ( clienteAtual.agendastatus != 'Trabalhando.' || status != 'Trabalhando.') {
        alert('Estoque não pode ser preenchido no status: '+clienteAtual.agendastatus);
        return;
    }

    var Inputselect = '';
    var selecao = $("#materiais option:selected").html();
    var selecaoVal = $("#materiais").val();
    if ($("#quantidade").val()) {
        var quant = $("#quantidade").val(); 
    } else {
        Materialize.toast('Preencha a quantidade/patrimônio', 2000);
        return;
    }

    Inputselect = '<div class="input-field divEstoque"><p data-valor="'+selecaoVal+'" class="inputdelete col s6" type="text" style="margin: 15px 15px 15px 15px; border-bottom: 1px solid grey; background-color: #f2f2f2;">'+selecao+'</p>';
    Inputselect += '<input value="'+quant+'" class="col s4" style="height: 2.555rem;" type="number"></div>';

    $("#registro").append(Inputselect);

    $(".inputdelete").each(function(){
        $(this).click(function(){
           Inputdelete = $(this).parent("div");
           $('#mostraItemExcluir').text(Inputdelete.children('p').text());
           $('#modalCancel').modal('open');
       });
    });
    salvarEstoque();
    $("#quantidade").val('');
});

$('#limpaEstoque').click(function () {
    if (storage.getItem(clienteAtual.agendaid)) {
        var est = JSON.parse(storage.getItem(clienteAtual.agendaid));
        var est = est.estoque;
        renovaEstoque();
    }
})

function envEstoque() {
    var est = 0;
    if (storage.getItem(clienteAtual.agendaid)) {
        var est = JSON.parse(storage.getItem(clienteAtual.agendaid));
        if (typeof(est.estoque) != 'undefined') {
            var est = est.estoque.tipo;
            console.log(est);
            if (est.consumo != 'novo' || est.retorno != 'novo') {
                var confere = window.confirm('Deseja fazer mais 1 envio?');
                if (!confere) {
                    return;
                }
            }
        }
    } else {
        Materialize.toast('Necessário selecionar um cliente', 2000);
        return;
    }

    var itens = '';

    $.each(estoque.resposta, function (index, value) {
        itens += value.texto+': '+value.valor+'\n';
    })

    var conf = window.confirm("Quer enviar o estoque com os seguintes itens?\n"+itens);
    
    if (conf) {
        $("#sendEstoque").prop('disabled', true);
        if ( typeof($('#consumo:checked').val()) == 'string' ) {
            estoque.tipo.consumo = 'preenchido';
        } else {   
            estoque.tipo.retorno = 'preenchido';
        }

        $.ajax({
            url: urlEstoque,
            method: 'post',
            data: estoque,
            success: function (arg) {
                $("#sendEstoque").prop('disabled', false);
                var resp = JSON.parse(arg);
                var string = '';
                if (resp.status == 'sucesso') {
                    clienteAtual = JSON.parse(storage.getItem(clienteAtual.agendaid));
                    if ( typeof(clienteAtual.estoque) != 'undefined' ) {
                        if (clienteAtual.estoque.tipo.retorno != 'novo') {
                            estoque.tipo.retorno = clienteAtual.estoque.tipo.retorno;
                        }
                        if (clienteAtual.estoque.tipo.consumo != 'novo') {
                            estoque.tipo.consumo = clienteAtual.estoque.tipo.consumo;
                        }
                    }

                    if (estoque.tipo.retorno != 'novo') {
                        var string = "retorno";
                    }
                    if (estoque.tipo.consumo != 'novo') {
                        if (string == '') {
                            var string = "consumo";
                        } else {
                            string += " e consumo";
                        }
                    }

                    $('#informacao').html('Estoque de '+string+' enviado(s).')
                    clienteAtual.estoque = estoque;
                    storage.setItem(clienteAtual.agendaid, JSON.stringify(clienteAtual));
                    Materialize.toast(resp.cadastrados.length+' itens inseridos com sucesso', 3000);
                    $("#enviaOk").prop('disabled', false);
                    $("#enviaWar").prop('disabled', false);
                }
            }, 
            error: function (arg) {
                if ( typeof($('#consumo:checked').val()) == 'string' ) {
                    estoque.tipo.consumo = 'novo';
                } else {   
                    estoque.tipo.retorno = 'novo';
                }
                $("#sendEstoque").prop('disabled', false);
                Materialize.toast('Houve um erro no envio do Estoque, tente novamente!', 3000);
                console.log(arg);
            }
        })
    }
}

const preencheEstoque = (estoqueAtual,id) =>{
    var conteudo = '';
    $.each(estoqueAtual, function(idx,val){
        conteudo += '<option value="'+val.id+'">'+val.nome+'</option>';
    })
    $('#'+id).html(conteudo);
}

const carregaItens = () => {
    let estoqueAtual = [];
    $.ajax({
      url:itensEstoque,
      method:'post',
      dataType: 'json',
      data: {funcao:'listaItens',id:credenciais.idLider},
      success: arg => {
        console.log(arg);
        if (arg == '') {
            alert('Estoque vazio, favor entrar em contato com o Almoxarifado responsável.');;
            return;
        }
        for (var i = arg.length - 1; i >= 0; i--) {
          estoqueAtual.push({
            id: arg[i].modid,
            nome: arg[i].mod_nome,
            qtd: arg[i].item_qtd
          })
        }
        preencheEstoque(estoqueAtual,'materiais');
      },
      error: arg => {
        console.log(arg);
      }
    })
}

const carregaItensGeral = () => {
    let estoqueAtual = [];
    $.ajax({
      url:almoxModelo,
      method:'post',
      dataType: 'json',
      data: {funcao:'listMobile'},
      success: arg => {
        var conteudo;
        console.log(arg);
        if (arg == '') {
            alert('Estoque vazio, favor entrar em contato com o Almoxarifado responsável.');;
            return;
        }

        $.each(arg.modelos, function(idx,val){
            conteudo += '<option data-cat="'+val.categoriaid+'" value="'+val.modid+'">'+val.mod_nome+'</option>';
        });
        $('#listaMateriais').html(conteudo);
        var conteudo = '';
        conteudo += '<option data-cat="todas" value="0">Listar Tudo</option>';
        $.each(arg.categorias, function(idx,val){
            conteudo += '<option value="'+val.categoriaid+'">'+
              val.categoria_nome+'</option>';
        });
        $('#listaCategorias').html(conteudo);
        arrumaSelectMateriais();
        
      },
      error: arg => {
        console.log(arg);
      }
    })
}
const arrumaSelectMateriais = () =>{
  var id = $('#listaCategorias').val();

  $('#listaMateriais option').each(function(idx,val){
    var cat = $(val).data('cat');
    if (cat == id || id == 0) {
      val.style.display = 'block';
      val.style.selected = true;
    } else{
      val.style.display = 'none';
    }
  });
}

const clickToAdd = (ids) =>{

    if ( typeof(clienteAtual.agendaid) == 'undefined') {
        Materialize.toast('Necessário selecionar um cliente', 2000);
        return;
    } else {
        estoque.cliente = clienteAtual.agendaid;
        estoque.circ = clienteAtual.agendacircuito;
    }

    // if ( clienteAtual.agendastatus != 'Trabalhando.' || status != 'Trabalhando.') {
    //     alert('Estoque não pode ser preenchido no status: '+clienteAtual.agendastatus);
    //     return;
    // }

    var Inputselect = '';
    var selecao = $("#"+ids.lista+" option:selected").html();
    var selecaoVal = $("#"+ids.lista).val();
    if ($("#"+ids.qtd).val()) {
        var quant = $("#"+ids.qtd).val(); 
    } else {
        Materialize.toast('Preencha a Quantidade', 2000);
        return;
    }

    Inputselect = '<div class="input-field divEstoque"><p data-valor="'+selecaoVal+'" class="inputdelete col s6" type="text" style="margin: 15px 15px 15px 15px; border-bottom: 1px solid grey; background-color: #f2f2f2;">'+selecao+'</p>';
    Inputselect += '<input value="'+quant+'" class="col s4" style="height: 2.555rem;" type="number"></div>';

    $("#registro").append(Inputselect);

    $(".inputdelete").each(function(){
        $(this).click(function(){
           Inputdelete = $(this).parent("div");
           $('#mostraItemExcluir').text(Inputdelete.children('p').text());
           $('#modalCancel').modal('open');
       });
    });
    salvarEstoque();
    $("#quantidade").val('');

}