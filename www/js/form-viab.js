function approach(valor){
	switch(valor){
		case "fibra":
			$('#fibraField').removeClass('hide');
			$('#inpField').removeClass('hide');
			break;
		case "radio":
			$('#fibraField').addClass('hide');
			$('#inpField').removeClass('hide');
			break;
	}
}
function selecFibra(valor) {
	switch(valor){
		case "outro":
			$('#fibraDivOutro').removeClass('hide');
			$('#fibraOutro').val('');
			break;
		default:
			$('#fibraDivOutro').addClass('hide');
			$('#fibraOutro').val('');
			break;
	}
}

var abordagem = "";

$('#anexarViab').click(function (e) {
	abordagem = $('#abordagem').val();
	tempEst = $('#estimadoSelect').val();

	if ( abordagem == "" ) {
		Materialize.toast('Escolha o tipo de abordagem!', 4000);
		return;
	}

	if ( tempEst == "0" ) {
		Materialize.toast('Escolha o tempo estimado!', 4000);
		return;
	}


	var dados = {
		abordagem: abordagem,
		estimadoSelect: tempEst
	};

	var foutro = $('#fibraOutro').val();

	if (dados.abordagem == "fibra") {
		if (foutro == "" || typeof(foutro) == "undefined" ) {
			if ($('#fibraSelect').val() != "0") {
				dados.fibraSelect = $('#fibraSelect').val();
			} else {
				Materialize.toast('Escolha a forma de entrega!', 4000);
				return;
			}
		} else {
			dados.fibraOutro = foutro;
		}
	}

	dados.condI = $('#condI').val();
	dados.condE = $('#condE').val();
	dados.materiais = $('#materiais').val();

	var testVazio = false;

	$.each( dados, function( key, value ) {
		$('#'+key).css("backgroundColor", "white");
		if ( value == "" || typeof(value) == "undefined" ) {
			testVazio = true;
			$('#'+key).css("background-color", "rgba(200,0,0,0.1)");
		}
	});
	var resultadoFinal = "";
	if (testVazio) {
		Materialize.toast("Favor preencher todos os campos destacados", 4000);
	} else {
		resultadoFinal = "Formulário de Viabilidade: \n";
		$.each( dados, function( key, value ) {
			resultadoFinal += preencheForm(key, value);
		});
		$('#obsFim').val(resultadoFinal);
		$('#modalRespViab').modal('close');
		Materialize.toast("Formulário anexado com sucesso!", 4000);
	}
});

function preencheForm(key, value) {
	switch(key){
		case "abordagem":
			return "Abordagem: " + value +"\n";
			break;
		case "fibraSelect":
		case "fibraOutro":
			return "Forma de entrega: " + value +"\n";
			break;
		case "condI":
			return "Condições Internas: " + value +"\n";
			break;
		case "condE":
			return "Condições Externas: " + value+"\n";
			break;
		case "materiais":
			return "Materiais necessários: " + value+"\n";
			break;
		default:
			return value+"\n";
			break;
	}
}
