function listaPac(p) {
  $('#pacpon').html('<i id="load" class="fa fa-spinner fa-spin"></i>');
  $('#btnConsultaCaixa').prop('disabled', true);
  $.ajax({
    type: "POST",
    url: urlFttn,
    datatype: "json",
    data: {funcao: "listaFTTN"},
    success: (retorno) => {
      $('#btnConsultaCaixa').prop('disabled', false);
      var pac = JSON.parse(retorno);
      var contador = pac.length;
      var cliente = {
        lat: Number(storage.getItem('lat')),
        lng: Number(storage.getItem('lng'))
      }

      var cl = new google.maps.LatLng(storage.getItem('lat'),storage.getItem('lng'));
      var conteudo = "";
      for (var i = pac.length - 1; i >= 0; i--) {
        var coords = {
          lat: Number(pac[i].lat),
          lng: Number(pac[i].lng)
        }
        var dis = Number(dist(cliente, coords).split('m')[0]);
        var ocup = Number(pac[i].ocup);
        var res = Number(pac[i].res);
        var total = ocup + res;
        var block = pac[i].block;
        if (block == 't') {
          continue;
        }
        if (dis < 90) {

          if ( pac[i].reservas != null) {
            var reservas = JSON.parse(pac[i].reservas);
            for (var j = reservas.length - 1; j >= 0; j--) {
              console.log(reservas[j]);
              if (reservas[j] == clienteAtual.agendacircuito) {
                total--;
              }
            }
          }
          if (Number(total) >= 6) {
            conteudo += '<div class="col s12 " style="margin: 2px;background-color: #fcd4d4"><p class="center"><b><span class="selecionaNome">'+pac[i].nome+'</span></b> - Ocup: <span class="red-text">'+ocup+'</span> ';
            conteudo += 'Res: <span class="red-text">'+res+'</span></p></div>';
          } else {
            
            conteudo += '<div class="col s12" style="margin: 2px;background-color: #e2f9f0"><p class="center"><b><span class="selecionaNome">'+pac[i].nome+'</span></b> - Ocup: <span class="green-text">'+ocup+'</span> ';
            conteudo += 'Res: <span class="green-text">'+res+'</span><br>';
            conteudo += '<b>Endereço: <b>'+pac[i].ende+'</b>';
            conteudo += ' Nº Atendimento: <b>'+pac[i].rmin+' - '+pac[i].rmax+'</b></div>';
            conteudo += '<div class="center" style="margin: 2px;background-color: #e2f9f0"><a class="green-text btn white waves-effect waves-light" href="javascript:void(0)" onclick="novaPac(\''+pac[i].nome+'\')"><b>Selecionar Caixa</b></a></div>';
          } 
        }
      }
      if (conteudo == '') {
        var cont = $('p');
        cont.addClass('center');
        cont.addClass('red-text');
        cont.text('Sem caixa disponível');
        $('#pacpon').html(cont);
      } else {
        $('#pacpon').html(conteudo);
      }
    },
    error: ()=>{
      $('#btnConsultaCaixa').prop('disabled', false);
      alert('Houve um erro. Contate o Setor de Desenvolvimento');
    }
  });
}

function novaPac(pac) {
  $('#nomeDoneAll').remove();
  var icon = '<i id="nomeDoneAll" class="material-icons left>done_all</i>"';
  $('.selecionaNome').each(function(){
    let nome = $(this);
    nome.css('color', 'black');
    if (nome.text() == pac) {
      nome.prepend(icon);
      nome.css('color', 'blue');
      Materialize.toast('Reserva alterada',2000);
    }
  })
  clienteAtual.agendapac = pac;
  console.log('agendapac:', clienteAtual.agendapac);
}
