function atualizaDBPOP() {
    var reqPop = window.indexedDB.open("listapop", storage.getItem("versaoDB"));

    dbAtualizando = false;
    reqPop.onupgradeneeded = function(e) {
        dbAtualizando = true;
        console.log('Atualizando DB');
        var dbp = e.target.result;
        if ( !dbp.objectStoreNames.contains('pops') ) {
            var oS = dbp.createObjectStore("pops", { keyPath: "popid", autoIncremente: true });
            oS.createIndex("popnome", "popnome", { unique: false });
            oS.createIndex("poplat", "poplat", { unique: false });
            oS.createIndex("poplng", "poplng", { unique: false });
        }
    };

    reqPop.onerror = function(e) {
      alert('Erro ao abrir o Banco de dados de POPs: Linha 18 dbpop.js\n Avisar ao programador.');
    };

    reqPop.onsuccess = function(e) {
        console.log('Abrindo o DB de POPs');
        bancoPOP = e.target.result;
        if (dbAtualizando) {
            preenchePOP(true);    
        }
        mostraPOPs();
    };
}

// popular DB a cada atualização de Banco de dados
function popDB(urlRadios, regiao) {
    $.ajax({
        timeout: 4000,
        async: false,
        method: "POST",
        url: urlRadios,
        datatype: "html",
        data: {funcao: "listaPOPDB", regiao: regiao}
    }).done( function(listaPOPs){
        console.log('lista de POPs atualizada');
        listadePOP = JSON.parse(listaPOPs);
    }).fail(function () {
        console.log("usando lista de POPs offline");
    });
}

function preenchePOP(atualizar) {
    if (atualizar) {
        popDB(urlRadios, regiao);
        var objStore = bancoPOP.transaction(["pops"], "readwrite").objectStore("pops");
        for (var i = 0; i < listadePOP.length; i++) {
            var obj = objStore.add(listadePOP[i]);
            obj.onsuccess = function(e) {
                console.log('OnUpgradeNeeded: POP Adicionado ao Banco de dados');
            }
            obj.onerror = function(e) {
                //Materialize.toast("Houve um erro na Atualização do Banco de dados de POPs.\n Favor avisar ao programador");
                console.log("Houve um erro na Atualização do Banco de dados de POPs.\n Favor avisar ao programador");
                console.log("OnUpgradeNeeded: "+e.target.error);
            }
        };
    }
}

function mostraPOPs() {
    var mc = "";
    var mcount = 0;
    var tx = bancoPOP.transaction(['pops'], 'readonly');
    var store = tx.objectStore('pops');
    var index = store.index('popnome');
    index.openCursor().onsuccess = function (e) {
        var cursor = e.target.result;
        if (cursor) {
            mc += "<li>";
            mc += "<input name='group1' class='visada' type='radio' id='"+cursor.value.popnome+"' data-lati='"+cursor.value.poplat+"' data-longi='"+cursor.value.poplng+"'/>";
            mc += "<label for="+cursor.value.popnome+">"+cursor.value.popnome+"</label>";
            mc += "</li>";
            cursor.continue();
        }
        $('#montaPop').html(mc);

    }
    index.openCursor().onerror = function (e) {
        console.log("Houve um erro no Banco de dados de POPs.\n Favor avisar ao programador");
    }
}
