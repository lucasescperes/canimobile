$('#pausar').click(function(){
  
  var desc = $('#descPausa').val();
  
  if (!desc || desc == "") {
      Materialize.toast('Descrição Obrigatória!', 3000);
      return;
  }

  atualizaStatus('Em Pausa.', desc)

  $('#modalDescPausa').modal('close');
  $('#descPausa').val('');

  $('#setores').html('');
  $('#valForm').html('');
  $('#coords').html('');
  
  status = 0;
  $("#os").removeClass("hide");
  $('#btnMove').removeClass("hide");
  $('#atualizaOs').removeClass('hide');
  $('#btnSearch').removeClass('hide');
  
  $('#map').addClass('hide');
  $('#rOk').addClass('hide');
  $('#btnNok').addClass('hide');
  $('#btnOk').addClass('hide');
  $("#btnAtiva").addClass("hide");
  $('#bntFormCorp').addClass("hide");
  $('#pause').addClass('hide');
  $('.div-confirma').addClass('desaparece');
  $('#btnInicio').addClass('hide');
  $('#btnConsulta').addClass('desaparece');
  $('#btngps').addClass('hide');
  $('#hideMap').addClass('hide');
  atualizaLista();
});