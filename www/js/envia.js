// $('#btnConsulta').on('click',function (){
function setores() {
    console.log("entrei no setores")
    $('.ssids').each(function () {
        $(this).click(function () {
            popLat = "";
            popLng = "";
            pop = "";

            $('#screen').html('');
            if (clienteAtual.agendatipo == "Viabilidade") {
                $('#bntFormCorp').removeClass("hide");
            }

            //var selec = 0;

            $('.ssids').each(function () {
                if (this.checked) {
                    popLat = this.getAttribute("data-lati");
                    popLng = this.getAttribute("data-longi");
                    pop = this.id;
                    //selec++;
                }
            });
            // if ( selec > 1 ) {
            //     $('#screen').html("<div id=\"card-alert\" class=\"card red\"><div class=\"card-content white-text\"><p><i class=\"mdi-alert-error left\"></i> Selecione apenas 1 SSID por vez</p></div></div>");
            //     return;
            // } else if ( selec < 1 ) {
            //     $('#screen').html("<div id=\"card-alert\" class=\"card red\"><div class=\"card-content white-text\"><p><i class=\"mdi-alert-error left\"></i> Selecione 1 SSID</p></div></div>");
            //     return;
            // }

            var POP = new google.maps.LatLng(popLat, popLng);
            var cliente = new google.maps.LatLng(lat, lng);

            distancia = google.maps.geometry.spherical.computeDistanceBetween(cliente, POP);

            var radio = "<div id=\"card-alert\" class=\"card red\"><div class=\"card-content white-text\"><p><i class=\"mdi-alert-error left\"></i> Distância excede limite máximo!</p></div></div>";

            if ( distancia <= 650 ) {
                var radio = "NanoBeam 16";
                var powRadio = 34;
                var esperado = ( powRadio - (20 * Math.log10(distancia) + 20 * Math.log10(5800) - 27.55 ) + 16 );
                sinalEsperado = Math.floor(esperado);

            } else if( distancia > 650 && distancia < 2500 ) {
                var radio = "PowerBeam 400";
                var powRadio = 43;
                var esperado = ( powRadio - (20 * Math.log10(distancia) + 20 * Math.log10(5800) - 27.55 ) + 16 );
                sinalEsperado = Math.floor(esperado);

            } else {
                $('#screen').html(radio);
                sinalEsperado = "Excede distância limite"; 
                return 1;
            }

            $('#screen').html("Rádio: <strong>"+radio+"</strong><br>");
            $('#screen').append("Sinal estimado: <strong>" + sinalEsperado +"dBm</strong><br>");

            clienteAtual = JSON.parse(storage.getItem(clienteAtual.agendaid));
            clienteAtual.POP = pop;
            clienteAtual.distancia = Math.ceil(distancia);
            clienteAtual.sinalEsperado = Math.ceil(esperado);
            storage.setItem(clienteAtual.agendaid, JSON.stringify(clienteAtual));
        });
    }); // Fim btnconsulta
}


// Enviar para a Fila
// $('.btnconfirma').on('click', function(){

//     if (storage.getItem('internet') == "OFF" || checkConnection() == 'Sem Internet') {
//       Materialize.toast("Certifique-se de ter internet para entrar na Fila",4000);
//       return; 
//     }

//     if (pop == "" || sinalEsperado == "" || distancia == "" ) {
//         Materialize.toast("Selecione ao menos 1 POP",4000);
//         return;
//     }

//     $('.btnconfirma').prepend('<i id="load" class="fa fa-spinner fa-spin left"></i>');
//     $('.btnconfirma').prop('disabled', true);    
//     fila('noc',true);
// });
function filaOS(tipoFila) {

    if (storage.getItem('internet') == "OFF" || checkConnection() == 'Sem Internet') {
      Materialize.toast("Certifique-se de ter internet para entrar na Fila",4000);
      return; 
    }

    $('.btnconfirma').prepend('<i id="load" class="fa fa-spinner fa-spin left"></i>');
    $('.btnconfirma').prop('disabled', true);    
    fila(tipoFila,true);
}

$('#btnEncerra').on('click', function(){
    openNav();
    $('#map').addClass('hide');
    $('#hideMap').addClass('hide');
    $('#btnNok').removeClass('hide');
    $('#btnOk').removeClass('hide');
    $("#rOk").removeClass("hide");
    $('#pause').addClass('hide');
    $('#screen').html('');
    $('#valForm').html('');
    $('#detail').html('');
    $(".btnconfirma").addClass("desaparece");
    $('#coords').html('');
    $("#btnEncerra").addClass("hide");
    $("#btnAtiva").addClass("hide");
    $("#listaos").addClass("hide");
    $(".div-confirma").addClass("hide");
    $("#btngps").addClass("hide");
    $('#setores').html('');
    if (typeof(clienteAtual.estoque) != 'undefined' ) {
        if (clienteAtual.estoque.tipo.consumo != 'novo' || clienteAtual.estoque.tipo.retorno != 'novo') {
            $("#enviaOk").prop('disabled', false);
            $("#enviaWar").prop('disabled', false);
        }
    }
    if ( clienteAtual.agendatipo.indexOf("Controle") != -1 || clienteAtual.agendatipo.indexOf("Manutenção") != -1 || clienteAtual.agendatipo.indexOf("Ativação") != -1 || clienteAtual.agendatipo.indexOf("Refazer") != -1 || clienteAtual.agendatipo.indexOf("Mudança") != -1 || clienteAtual.agendatipo.indexOf("Viabilidade") != -1 ) {
        $("#enviaOk").prop('disabled', false);
        $("#enviaWar").prop('disabled', false);
    }
});

// chamado como = fila('noc',true) - em um estagio da OS
// chamado como = fila('noc') - no Modal
// chamado como = fila('infra') - no Modal
function fila(fila, finaliza) {

  var dados = {
    sinal: '',
    distancia: '',
    visada: '',
    latitude: '',
    longitude: '',
    agendaid: '',
    fila: fila,
    idTec: credenciais.idLider,
    idEqp: credenciais.eqpid,
    numero: credenciais.fone
  };
    
  if (typeof lat != 'undefined') {
    dados.latitude = lat;
    dados.longitude = lng;
  }

  if (typeof clienteAtual != 'undefined' && clienteAtual != 'undefined' && clienteAtual != '') dados.agendaid = clienteAtual.agendaid;
  if (sinalEsperado) dados.sinal = sinalEsperado;
  if (distancia) dados.distancia = distancia;
  if (pop) dados.visada = pop;
  console.log(dados);
  $.ajax({
    method: "POST",
    url: urlFila,
    timeout: 4000,
    data: dados,
    success: function (arg) {
      $('#load').remove();
      console.log(arg);
      $('.btnconfirma').prop('disabled', false);
      if (finaliza) {
        $('#btnInicio').addClass('hide');
        $('#btnEncerra').removeClass('hide');
        $('#btnEstoque').removeClass("hide");
      }
      Materialize.toast(arg.posicao, 2000);
    },
    error: function (argument,erro) {
      $('#load').remove();
      Materialize.toast("Erro ao recuperar posição na fila." , 2000);
      $('.btnconfirma').prop('disabled', false);
      console.log(argument);
      console.log(erro);
    }
  });
}


function enviaResposta(id) {

    if ( $('#selecCode').val() == "0" ) {
      Materialize.toast("Favor preencher Código de Encerramento.",2000);
      return;
    }

    if ( $('#respCode').val() == "0" ) {
      Materialize.toast("Favor preencher Código de Resposta.",2000);
      return;
    }

    $('#'+id).prepend('<i id="load" class="fa fa-spinner fa-spin left"></i>');
    $('#'+id).prop('disabled', true);
    
    var dados = {
      funcao: 'inserir',
      codid: $('#selecCode').val(),
      respostaCod: $('#respCode').val(),
      agid: clienteAtual.agendaid,
      eqpid: credenciais.eqpid,
      desc: $('#obsFim').val(),
      tipo: id
    }

    if (id == 'enviaInv') {
        console.log(id);
    } else {

    var est = JSON.parse(storage.getItem(clienteAtual.agendaid));

        switch (clienteAtual.agendatipo){
            case "Instalação":
            case 'Instalação RF':
            case 'Instalação Fibra N':
            case 'Instalação Fibra Cond':
            case 'Instalação Fibra MAX':
            case "Instalação Banda Larga":
            case "Instalação Temporada":
            case "Instalação Temporada - Telefone":
            case "Instalação Temporada - Wifi":
            case "Instalação Temporada - Telefone - Wifi":
            case "Transferência Temporada":
            case "Transferência Temporada - Telefone":
            case "Transferência Temporada - Wifi":
            case "Transferência Temporada - Telefone - Wifi":
            case "Instalação Transferência":
            case "Instalação Transferência - Telefone":
            case "Instalação Transferência - Wifi":
            case "Instalação Transferência - Telefone - Wifi":
            case "Ponto extra Temporada":
            case "Ponto extra Temporada - Telefone":
            case "Ponto extra Temporada - Wifi":
            case "Ponto extra Temporada - Telefone - Wifi":
            case "Instalação Combo Rádio":
            case "Instalação Combo Rádio - Telefone":
            case "Instalação Combo Rádio - Wifi":
            case "Instalação Combo Rádio - Telefone - Wifi":
            case "Ativação Telefone":
            case "Ativação Telefone - Telefone":
            case "Ativação Telefone - Wifi":
            case "Ativação Telefone - Telefone - Wifi":
            case "Ativação Wifi":
            case "Ativação Wifi - Telefone":
            case "Ativação Wifi - Wifi":
            case "Ativação Wifi - Telefone - Wifi":

                if (typeof(est.estoque) == 'undefined') {
                    alert('Estoque de consumo ainda não enviado!');
                    $('#load').remove();
                    $('#'+id).prop('disabled', false);
                    return;

                } else {
                    var est = est.estoque.tipo;
                    console.log(est);
                    if (est.consumo == 'novo') {
                        alert('Estoque de consumo Salvo, mas ainda não enviado!');
                        $('#load').remove();
                        $('#'+id).prop('disabled', false);
                        return;
                    }
                }
                break;
            case "Instalação Combo Ethernet - Migração":
                if (typeof(est.estoque) == 'undefined') {
                    alert('Estoque ainda não enviado!');
                    $('#load').remove();
                    $('#'+id).prop('disabled', false);
                    return;
                } else {
                    var est = est.estoque.tipo;
                    console.log(est);
                    var string = '';
                    if (est.retorno == 'novo') {
                        var string = "retorno";
                    }
                    if (est.consumo == 'novo') {
                        if (string == '') {
                            var string = "consumo";
                        } else {
                            string += " e consumo";
                        }
                    }
                    if (string != '') {
                        alert('Estoque de '+string+' Salvo(s), mas ainda não enviado(s)!');
                        var confirma = false;
                        if (est.consumo == 'preenchido') {
                            var confirma = window.confirm('Enviar sem estoque de '+string);
                        }
                        if (!confirma) {
                            $('#load').remove();
                            $('#'+id).prop('disabled', false);
                            return;
                        }
                    }
                }
                break;

            case "Desinstalação Temporada":
            case "Desinstalação Transferência":
            case "Desinstalação Inadimplência":
            case "Desinstalação por Opção":
                if (typeof(est.estoque) == 'undefined') {
                    alert('Estoque de retorno ainda não enviado!');
                    $('#load').remove();
                    $('#'+id).prop('disabled', false);
                    return;
                } else {
                    var est = est.estoque.tipo;
                    console.log(est);
                    if (est.retorno == 'novo') {
                        alert('Estoque de retorno Salvo, mas ainda não enviado!');
                        $('#load').remove();
                        $('#'+id).prop('disabled', false);
                        return;
                    }
                }
                break;
            default:
                $.post(reportProblem,{nome:'Decisão do Estoque',desc:'Tipo de OS Desconhecida',linha:'331',arq:'envia.js',adicionais:[clienteAtual,credenciais]});
                break;
        }
        if ( (typeof(clienteAtual.agendavalor) == 'null' && clienteAtual.agendavalor == 'null')) {
            dados.valor = clienteAtual.agendavalor;
        }
    }

    $.ajax({
        url: urlResp,
        method: 'post',
        timeout: 10000,
        data: dados,
        success: function (arg) {
            var resp = JSON.parse(arg);
            if ( resp.status == 'sucesso') {

                Materialize.toast(resp.msg, 3000);
                
                deleteHist(clienteAtual.agendaid);
                storage.removeItem(clienteAtual.agendaid);
                renovaEstoque();
                atualizaLista();
                voltaAtualiza();
            }
            
        },
        error: function () {

            // alert('Resposta não enviada, favor tentar novamente!');
            clienteAtual = JSON.parse(storage.getItem(clienteAtual.agendaid));
            clienteAtual.fim = horaAtual();
            clienteAtual.agendastatus = "Concluído.";
            clienteAtual.pendente = "pendenteEnvio";
            clienteAtual.idBTN = id;
            clienteAtual.codid = $('#selecCode').val();
            clienteAtual.desc = $('#obsFim').val();

            storage.setItem(clienteAtual.agendaid, JSON.stringify(clienteAtual));

            // insere objeto no banco
            atualizabancoHist(clienteAtual);
            atualizaListaOsUnica(clienteAtual.id);
            renovaEstoque();
            console.log("ID no ESTOQUEJS "+ clienteAtual.id);
            atualizaLista();
            voltaAtualiza();

        }
    })
}

function enviaOFF(id) {
    $('#sinc').prop('disabled', true);
    $('#sinc').prepend('<i id="load" class="fa fa-spinner fa-spin left"></i>');
    clienteAtual = JSON.parse(storage.getItem(id));
    $('#selecCode').val(clienteAtual.codid);
    $('#obsFim').val(clienteAtual.desc);
    estoque = clienteAtual.estoque;
    enviaResposta(clienteAtual.idBTN);
}