

$('#botaoEntrar').click(function() {
    $('#botaoEntrar').prepend('<i id="load" class="fa fa-spinner fa-spin left"></i>');
    var usuario = $('#login').val();
    var senha = $('#password').val();

    $.ajax({
        url: urlLogin,
        method: 'post',
        data: {
            usuario: usuario,
            senha: senha,
        },
        success: function (arg) {
            $('#loader-wrapper').fadeOut( "slow" );
            $('#loader').fadeOut( "slow" );
            $('#load').remove();
            var resp = JSON.parse(arg);
            if (resp.status == 'erro') {
                Materialize.toast(resp.msg, 3000);
                return;
            }
            credenciais = {
                equipe: resp.credenciais.eqpnome,
                loja: resp.credenciais.lider_loja_nome,
                lojaid: resp.credenciais.lider_loja,
                eqpid: resp.credenciais.eqpid,
                regiao: resp.credenciais.eqpregiao,
                idLider: resp.credenciais.idLi,
                idMembro: resp.credenciais.idMemb,
                fone: resp.credenciais.eqpfone
            }

            // linha alimenta o objeto "estoque" para ser enviado posteriormente
            renovaEstoque();
            carregaItens();
            carregaItensGeral();
            
            // Salva Usuário e Senha para autopreenchimento CASO sucesso no login
            storage.setItem('login',usuario);
            storage.setItem('password',senha);

            $("#os").removeClass("hide");
            // chamaAtualiza();
            atualizaLista();
            chamaDBhist();
            
            // atualizaOs(urlOs, equipeID);
            $("#pagForm").addClass("desaparece");
            $("#msgBox").removeClass("hide");
            $("#estoque").removeClass("hide");
            $("#navMain").removeClass("hide");
            $("#menuSlide").removeClass("hide");
            $("#tabMain").removeClass("hide");
            $(".rodape").removeClass("hide");
            $("#cameraPequena").removeClass("hide");
            $("#phone").removeClass("hide");
            $("#contatoRG").removeClass("hide");

            rastreamento();

        }, 
        error: function (arg) {
             $('#loader-wrapper').fadeOut( "slow" );
            $('#loader').fadeOut( "slow" );
            Materialize.toast('Login ou Senha inválido!',3000);
        }
    });
});


// function listadePops(urlRadios, regiao) {
//     $.ajax({
//         method: "POST",
//         url: urlRadios,
//         datatype: "html",
//         data: {funcao: "novoListaPOP", regiao: regiao}
//     }).done( function(listaPOPs){
//         $('#montaPop').html(listaPOPs);
//     }).fail(function (argument, teste) {
//         alert(argument +"\n"+ teste);
//     });
// }
