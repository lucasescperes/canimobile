// funcao permissao
function gpsPermissoes(){
    navigator.geolocation.getCurrentPosition(gpsPermOk, gpsPermError, {maximumAge: 6000, timeout: 15000, enableHighAccuracy: true} );
};

function gpsPermOk(pos) {
    storage.setItem('lat',pos.coords.latitude);
    storage.setItem('lng',pos.coords.longitude);
    console.log(pos);
    // rastreamento();
}

function gpsPermError() {
    storage.setItem('lat', -32.058545);
    storage.setItem('lng', -52.147350);
    // rastreamento();
}

// fim funcao permissao

// Funções relacionadas ao uso de GPS
$("#btngps").on('click', function () {
    Materialize.toast('Usando coordenadas atuais',3000);
    lat = storage.getItem('lat');
    lng = storage.getItem('lng');
//     $('#map').removeClass('hide');
//     $('#hideMap').removeClass('hide');
//     initMap(Number(lat),Number(lng));
    retSet();
});

//RASTREAMENTO
function trackingS(position) {
    navigator.geolocation.clearWatch(watchID);
    var coordsT = {
        funcao: 'tracking',
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        accu: position.coords.accuracy,
    }
    
    var ponto = new google.maps.LatLng(storage.getItem('lat'), storage.getItem('lng'));
    var novoPonto = new google.maps.LatLng(coordsT.latitude, coordsT.longitude);
    var distanciaEntre = google.maps.geometry.spherical.computeDistanceBetween(ponto, novoPonto);
    console.log(distanciaEntre);
    if (distanciaEntre < 15000) {
        if (distanciaEntre > 50) {
            if (credenciais) {
                coordsT.equipe = credenciais.eqpid;
                $.post(urlTracking,{dados:coordsT});
            }
        }
    }

    storage.setItem('lat',coordsT.latitude);
    storage.setItem('lng',coordsT.longitude);
    var precisao = Math.floor(coordsT.accu);
    storage.setItem('accu',precisao);
    $('#divCoords').html(coordsT.latitude + " " +coordsT.longitude + "<br>Precisão: "+ precisao+"m");

}

function trackingE(error) {
    rastreamento();
}

function rastreamento(){
    cordova.plugins.diagnostic.getLocationMode(
        function(locationMode){
            if (locationMode == cordova.plugins.diagnostic.locationMode.LOCATION_OFF){
                alert('Por favor, ative o GPS para continuar usando o APP');
                cordova.plugins.diagnostic.switchToLocationSettings();
            }
        },
        function(error){
            console.error("The following error occurred: "+error);
        }
    );

    watchID = navigator.geolocation.getCurrentPosition(trackingS, trackingE, { enableHighAccuracy: true});
}

// function sleep(ms) {
//   return new Promise(resolve => setTimeout(resolve, ms));
// }

// async function demo() {
//   await sleep(60000);
//   rastreamento();
//   console.log('demo');
// }

setInterval(rastreamento, 30000);

function updateCoordsSuccess(position) {
    
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    var accu = position.coords.accuracy;

    console.log('Precisão: '+accu);
    var precisao = Math.ceil(accu);
    if ( accu <= 40 ) {
        limpaUPDATE();
        storage.setItem('lat',lat);
        storage.setItem('lng',lng);
        storage.setItem('accu',accu);
        $('#divCoords').html(lat.toFixed(5) + " " +lng.toFixed(5) + " <br><strong>Precisão: "+ Math.floor(accu)+"m </strong>");
    } else {
        $('#divCoords').html(lat.toFixed(5) + " " +lng.toFixed(5) + " <br><strong>Precisão: "+ Math.floor(accu)+"m </strong> <i id='load' class='fa fa-spinner fa-spin'></i>");
    }
    if (contaTracker == 0 ) {
        storage.setItem('accu',accu);
        $('#divCoords').html(lat.toFixed(5) + " " +lng.toFixed(5) + " <br><strong>Precisão: "+ Math.floor(accu)+"m </strong>");
        alert('GPS não conseguiu melhorar a pesquisa');
        limpaUPDATE();
    } else {
        contaTracker--;
    }
    console.log('Tracker: '+contaTracker);
   
}

function updateCoordsError(error) {
    upC();
}

function limpaUPDATE(){
    $('#btnUpdt').prop('disabled', false);
    $('#btnUpdt').css('color', 'white');
    navigator.geolocation.clearWatch(updCoord);
    verificaUpc = true;
}

$('#btnUpdt').click((e)=>{
    e.preventDefault();
    $('#btnUpdt').prop('disabled', true);
    $('#btnUpdt').css('color', 'red');
    if (verificaUpc) {
        upC();
    }
})

verificaUpc = true;

function upC(){
    verificaUpc = false;
    Materialize.toast('Aprimorando coordenadas', 1500);
    updCoord = navigator.geolocation.watchPosition(updateCoordsSuccess, updateCoordsError, { enableHighAccuracy: true});
    contaTracker = 20;
}