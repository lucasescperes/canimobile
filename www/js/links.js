
// CONSTANTES
versaoAPP = 2.84;
status = 0;
sinalEsperado = "";
distancia = "";
pop = "";
storage = window.localStorage;
clienteAtual = {};
urlCani 			= 'https://canicontrol.vetorial.net/';
itensEstoque  = urlCani+'nova/api/almx/tecnico-movimentacao-Temp.php';
urlFttn       = urlCani+'api/fibra/geral.php';
almoxModelo   = urlCani+'almox/api/modelo.php';
almoxCat      = urlCani+'almox/api/categoria.php';
reportProblem = urlCani+'api/app/problems.php'; 
urlEstoque    = urlCani+'api/app/estoque.php';
urlStatus     = urlCani+'api/app/geral.php';
urlOs         = urlCani+'api/app/os.php';
urlLogin      = urlCani+'api/app/login.php';
urlUpdate     = urlCani+'api/app/config.php';
urlResp       = urlCani+'api/app/resposta.php';
urlAtiva      = urlCani+'api/app/ativa.php';
urlRadio      = urlCani+'app/geo.php';
urlAssinatura = urlCani+'app/save.php';
urlTracking   = urlCani+'app/tracking.php';
urlFotos      = urlCani+'app/foto.php';
urlFila       = "https://cani.vetorial.net:8081/tarefas/inserir";
