var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    resume: function () {
        console.log('resume');
    },
    pause: function () {
        console.log('pause');  
    },

    onDeviceReady: function() {

        $(".button-collapse").sideNav();
        $('.modal').modal();
        
        gpsPermissoes();

        //BANCO DE DADOS
        $.ajax({
            method: "POST",
            url: urlUpdate,
            timeout: 6000,
            //async: true
        }).done( function(versao){
            console.log(versao);
            var ver = JSON.parse(versao);
            storage.setItem("versaoAPP", ver.versaoAPP);
            checaversao();
            // Atualiza versão dos bancos de dados caso tenha internet.
            for (var k in ver){
                VarBanco(k, ver[k]);
            }
            // chamaDBhist();
            chamaDBos();
            $('#botaoEntrar').removeClass('disabled');
            
        }).fail(function (erro) {
            if (storage.getItem('Internet') == "ON") {
                console.log('falha ao buscar a versão do DB');    
            } else {
                if (storage.getItem('Internet') == "OFF") {
                    console.log('falha ao buscar a versão do DB: Sem internet.');
                } else {
                    console.log('falha ao buscar a versão do DB: Info sobre Internet não alcançáveis');
                }
            }
        });

        // Checa se já tem um login salvo e atribui ao campo de Usuário

        if ( storage.getItem('login') ) {
            $('#login').val(storage.getItem('login'));
        }

        if ( storage.getItem('password') ) {
            $('#password').val(storage.getItem('password'));
        }

        //seta versaoDB para 1 caso na hora da PRIMEIRA abertura do app não tenha internet
        if ( !storage.getItem("versaoDBPOP") ) {
            console.log("Versão iniciada em 1 para versaoDBPOP")
            storage.setItem("versaoDB", 1);
        }

        if ( !storage.getItem("versaoDBos") ) {
            console.log("Versão iniciada em 1 para versaoDBos")
            storage.setItem("versaoDBos", 1);
        }

        if ( !storage.getItem("versaoDBhist") ) {
            console.log("Versão iniciada em 1 para DBSync")
            storage.setItem("versaoDBhist", 1);
        } // FIM DOS DBS DE QUEM TENTA LOGAR PELA PRIMEIRA VEZ NO APP SEM INTERNET

        this.receivedEvent('deviceready');
        
        //escuta os eventos e atualiza o status de conexão com a internet
        document.addEventListener("offline", onOffline, false);
        document.addEventListener("online", onOnline, false);

        // altera a cor da barra de status do Android
        if (cordova.platformId == 'android') {
           StatusBar.backgroundColorByHexString("#1565C0");
        }

        document.addEventListener("backbutton", onBackKeyDown, false);

        function onBackKeyDown(e) {
            console.log('backbutton clicado');
        }
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        // var parentElement = document.getElementById(id);
        // var listeningElement = parentElement.querySelector('.listening');
        // var receivedElement = parentElement.querySelector('.received');

        // listeningElement.setAttribute('style', 'display:none;');
        // receivedElement.setAttribute('style', 'display:block;');

        // console.log('Received Event: ' + id);
    }
};

app.initialize();