function checaversao() {
    if ( Number(storage.getItem('versaoAPP')) == versaoAPP) {
        $('#footText').text("Versão "+versaoAPP);
    } else {
        console.log(Number(storage.getItem('versaoAPP')));
        console.log(storage.getItem('versaoAPP'));
        alert("Versão desatualizada. Favor baixar a última versão");
        navigator.app.exitApp();
        return;
    }
}

function dist(a, b) {
 var d = new google.maps.LatLng(b.lat, b.lng);
 var c = new google.maps.LatLng(a.lat, a.lng);
 var e = google.maps.geometry.spherical.computeDistanceBetween(c, d);
 return Math.ceil(e)+"m";
}

function retSet() {
    console.log('retSet');
    $('#setores').html('');
    $('#setores').removeClass("hide");
    $('.btnconfirma').removeClass("desaparece");
    $('#btngps').removeClass("hide");
    $('#btnEncerra').removeClass("hide");
    sinalEsperado = " ";
    distancia = " ";
    pop = "Sem setor indicado";

    if (clienteAtual.agendatipo.indexOf("Ethernet") != -1 || clienteAtual.agendatipo.indexOf("Desinstalação") != -1 || clienteAtual.agendatipo.indexOf("Ativação") != -1  ) {
        console.log('retornei');
        return;
    }
    console.log('não retornei');
    $.ajax({
        type: "POST",
        url: urlRadio,
        timeout: 6000,
        data: {
            funcao: "distro",
            pop: ""
        },
        success: function(retorno){
            coordSetor = JSON.parse(retorno);
            var contador = coordSetor.length ;
            var mc = "";
            for (var i = coordSetor.length - 1; i >= 0; i--) {
                var coords = {
                    lat: coordSetor[i].lat,
                    lng: coordSetor[i].lng
                }
                var cliente = {
                    lat: Number(lat),
                    lng: Number(lng)
                }
                if ( contem(new google.maps.LatLng(lat,lng), coordSetor[i].area) ) {
                    console.log(contem(new google.maps.LatLng(lat,lng), coordSetor[i].area))
                    if (coordSetor[i].block == 'f') {
                        mc += "<li>";
                        mc += "<input name='group2' class='ssids' type='radio' id='"+coordSetor[i].ssid;
                        mc += "' data-lati='"+coordSetor[i].lat+"' data-longi='"+coordSetor[i].lng+"'/>";
                        mc += "<label for="+coordSetor[i].ssid+">"+coordSetor[i].ssid+"</label>";
                        mc += "</li>";
                    } else {
                        mc += "<li>";
                        mc += "<input name='group2' class='ssids ' disabled='disabled' type='radio' id='"+coordSetor[i].ssid;
                        mc += "' data-lati='"+coordSetor[i].lat+"' data-longi='"+coordSetor[i].lng+"'/>";
                        mc += "<label for="+coordSetor[i].ssid+">"+coordSetor[i].ssid+" <span class='red-text'>Fechado!</span></label>";
                        mc += "</li>";
                    }
                } else {
                    contador--;
                    console.log(contador);
                }
            }
            if (contador == 0) {
                $('#setores').html('<span  class="red-text" ><strong>Sem setor indicado para esse cliente.</strong><span><br>');

                clienteAtual = JSON.parse(storage.getItem(clienteAtual.agendaid));
                clienteAtual.POP = pop;
                clienteAtual.distancia = distancia;
                clienteAtual.sinalEsperado = sinalEsperado;
                storage.setItem(clienteAtual.agendaid, JSON.stringify(clienteAtual));

            } else {
                console.log('else do retSet')
                console.log(mc)
                $('#setores').html(mc);
                setores();
            }
        },
        error: function () {
            $('#setores').html('Pesquisa por SSID Falhou devido a oscilação na internet. <br>Caso necessário, repita a operação.');
        }
    });
}

function contem(a, b) {
    var f = [];
    var j = b.replace(/\)/g,'|').replace(/\(/g,'').replace(/\|\|$/g,'').split('|');
    for (var i = 0; i < j.length; i++) {
        var g = j[i].replace(/^,/g,'').split(',')[0];
        var h = j[i].replace(/^,/g,'').split(',')[1];
        var e = new google.maps.LatLng(Number(h), Number(g));
        f.push(e);
    }
    var c = new google.maps.Polygon({path: f});
    var d = google.maps.geometry.poly.containsLocation(a, c);
    return d;
}

// Checa Internet Status
function onOnline() {
    $('#internet').addClass('hide');
    storage.setItem('Internet', "ON");
    console.log('on');
}
function onOffline() {
    $('#internet').removeClass('hide');
    Materialize.toast('Sem acesso a internet',10000);
    storage.setItem('Internet', "OFF");
    console.log('off');
}

//checa tipo de conexão

function checkConnection() {
    var networkState = navigator.connection.type;
    var states = {};
    states[Connection.UNKNOWN]  = 'Conexão desconhecida';
    states[Connection.WIFI]     = 'Conexão WiFi';
    states[Connection.CELL_2G]  = 'Conexão 2G';
    states[Connection.CELL_3G]  = 'Conexão 3G';
    states[Connection.CELL_4G]  = 'Conexão 4G';
    states[Connection.CELL]     = 'Conexão genérica';
    states[Connection.NONE]     = 'Sem Internet';
    return states[networkState];
}

function initMap(lat,lng) {
    map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: lat, lng: lng},
    zoomControl: true,
    mapTypeControl: true,
    scaleControl: true,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: true,
    styles: [
          {
            featureType: 'poi',
            stylers: [{visibility: 'off'}]
          },
          {
            featureType: 'transit',
            elementType: 'labels.icon',
            stylers: [{visibility: 'off'}]
          },{
            featureType: "landscape",
            elementType: "labels",
            stylers: [
              { "visibility": "off" }
            ]
          }
        ],
    gestureHandling: 'greedy'
    });

    var marker = new google.maps.Marker({
        position: {lat: lat, lng: lng},
        map: map,
    });

    map.setZoom(14);
    map.panTo(marker.position);
};

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

function VarBanco(nomebanco, versao) {
    if ( storage.getItem(nomebanco) ) {
        if ( Number(versao) > Number(storage.getItem(nomebanco)) ) {
            console.log('Nova versão do DB: '+nomebanco+" "+versao);
            storage.setItem(nomebanco, versao);
            if (nomebanco == "versaoDB") {
                indexedDB.deleteDatabase('listapop');
            }
        }
    } else {
        console.log("Banco de Dados "+versao+" OK");
    }
}


function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function horaAtual(){
    var d = new Date();
    var h = addZero(d.getHours());
    var m = addZero(d.getMinutes());
    var s = addZero(d.getSeconds());
    var dia = addZero(d.getDate());
    var mes = addZero(d.getMonth()+1);
    var ano = d.getFullYear().toString().substr(-2);
    return dia+"-"+mes+"-"+ano+" "+h + ":" + m + ":" + s;
}

function iniciaOs() {
    clienteAGID = "0";
    $('.valorOS').each(function() {
        $(this).click(function() {
            clienteAGID = this.value;
            var turno = this.getAttribute('data-data');
            lat =  this.getAttribute('data-lat');
            lng =  this.getAttribute('data-lng');
            var tipo = this.getAttribute('data-tipo');
            var banda = this.getAttribute('data-banda');
            status =  this.getAttribute('data-status');
            clienteAtual = {
                agendaid: clienteAGID,
                id: this.getAttribute('data-id'),
                agendanome: this.getAttribute('data-nome'),
                agendacpf: this.getAttribute('data-cpf'),
                agendaendereco: this.getAttribute('data-end'),
                agendalat: lat,
                agendalng: lng,
                agendatipo: this.getAttribute('data-tipo'),
                agendacontato: this.getAttribute('data-contato'),
                agendacontatot: this.getAttribute('data-contatot'),
                agendadesc: this.getAttribute('data-obs'),
                agendacircuito: this.getAttribute('data-circ'),
                agendasenhacirc: this.getAttribute('data-senhacirc'),
                agendapac: this.getAttribute('data-pac'),
                agendaformapag: this.getAttribute('data-formapag'),
                agendavalor: this.getAttribute('data-valor'),
                agendastatus: this.getAttribute('data-status'),
                ativado: false,
            }

            if (typeof(banda) != "undefined") {
                clienteAtual.agendabanda = banda;
            } else {
                clienteAtual.agendabanda = "Não informada.";
            }

            if (turno < 12) {
                var turno = 'Manhã';
            } else {
                var turno = 'Tarde';
            }
            clienteAtual.turno = turno;
            var ordens = '<strong>'+clienteAtual.agendanome+'</strong> ('+clienteAtual.agendaid+')</strong><br>';
            ordens += 'End: <strong>'+clienteAtual.agendaendereco+'</strong><br>Tipo: <strong>'+tipo+'</strong><br>';
            if (clienteAtual.agendapac != "null" && clienteAtual.agendapac != "" && clienteAtual.agendapac != '0' ) {
                ordens += 'Caixa: <b><span class="green-text">'+clienteAtual.agendapac+'</span></b><br>';
            }
            ordens += 'Circuito: <strong>'+clienteAtual.agendacircuito + '</strong> / Senha: <strong>'+clienteAtual.agendasenhacirc+'</strong><br>';
            ordens += 'Plano/Banda: <strong>'+clienteAtual.agendabanda+'</strong><br>'; 
            ordens += 'Turno: <strong>'+turno+'</strong> / Status: <strong>'+status+'</strong><br>Contato: <strong>'+clienteAtual.agendacontato+'</strong><br>';
            if (clienteAtual.agendacontatot != "" ) {
                ordens += 'Contato Tec: <strong>'+clienteAtual.agendacontatot+'</strong><br>';
            }
            if (clienteAtual.agendavalor != "null" && clienteAtual.agendavalor != "0") {
                ordens += '<b>Valor a receber: <span class="blue-text"> R$ '+clienteAtual.agendavalor+'</span></b><br>';
            }
            ordens += 'Obs.: <strong>'+clienteAtual.agendadesc+'</strong>';

            $('#detail').html(ordens);

            if ((lat < 0) && (lng < 0)) {
                $('#detail').append('<br>Coordenadas: <strong>'+lat+' '+lng+'</strong>');
            }
            console.log(clienteAtual);
            $('#btnMove').prop('disabled', false);
        });
    });
};

function mostraMapa() {
    if ( (lat < 0) && (lng < 0) ) {
        $('#map').removeClass('hide');
        initMap(Number(lat),Number(lng));
    }
}

function semCallNOC($assinatura) {
    if ($assinatura) {
        $('#pause').addClass('hide');
        $('#screen').html('');
        $('#valForm').html('');
        $('#detail').html('');
        $(".btnmostraf").addClass("desaparece");
        $(".btnconfirma").addClass("desaparece");
        $("#btnConsulta").addClass("desaparece");
        $("#listadePOPs").addClass("hide");
        $("#rOk").removeClass("hide");
        $('#coords').html('');
        $("#btnEncerra").addClass("hide");
        $("#btngps").addClass("hide");

    } else {
        $('.div-confirma').removeClass('desaparece');
        $('#btnEncerra').removeClass('hide');
        $('#detail').html('');
        $('#listadePOPs').addClass('hide');
    }
}

function deleteCaniApk() {
    var entry= "file:///storage/emulated/0/Android/data/net.vetorial.canicontrol/files/Download/CaniMobile.apk";
    window.resolveLocalFileSystemURL (entry, 
        function (fileEntry) { 
            fileEntry.remove(
                function () { 
                    console.log('CaniMobile.apk deletado');
                }, 
                function () {
                    console.log('erro ao deletar CaniMobile.apk');
                }
            ); 
        } 
    );
}

//limpa campos da aba estoque
// function resetCampos() {
//     $('form').each(function(){
//         this.reset();
//     });
// }

 // para Download
    // var ft = new FileTransfer();
    // ft.download(
    //   "http://canicontrol.vetorial.net/mapa.kml",
    //   cordova.file.externalApplicationStorageDirectory+'mapa.kml',
    //   function(entry) {
    //     console.log("success");
    //     console.log('download complete: ' + entry.toURL());

    //   },
    //   function(err) {
    //     alert('erro');
    //     alert('download error source ' + error.source);
    //     alert('download error target ' + error.target);
        
    //   }
    // );