
function chamaDBhist(){
    var reqHist = window.indexedDB.open("historicos", storage.getItem("versaoDBhist"));

    reqHist.onupgradeneeded = function(e) {
        var dbh = e.target.result;
        if ( !dbh.objectStoreNames.contains('historico') ) {
            var oS = dbh.createObjectStore("historico", { keyPath: "agendaid" });
            oS.createIndex("agendaid", "agendaid", { unique: true });
            oS.createIndex("data", "data", { unique: false });
            oS.createIndex("id", "id", { unique: false });
            oS.createIndex("agendanome", "agendanome", { unique: false });
            oS.createIndex("agendaendereco", "agendaendereco", { unique: false });
            oS.createIndex("agendatipo", "agendatipo", { unique: false });
            oS.createIndex("agendacircuito", "agendacircuito", { unique: false });
            oS.createIndex("agendapop", "agendapop", { unique: false });
            oS.createIndex("agendasinal", "agendasinal", { unique: false });
            oS.createIndex("agendastatus", "agendastatus", { unique: false });
            oS.createIndex("agendafim", "agendafim", { unique: false });
        }
    }

    reqHist.onerror = function(e) {
      alert('Erro ao abrir o Banco de dados de Histórico: Linha 22 dbos.js\n Avisar ao programador.');
    };

    reqHist.onsuccess = function(e) {
        console.log('Abrindo o DB de Históricos');
        bancoHist = e.target.result;
        // mostraHist();
    };
};

function atualizabancoHist(cliente) {
    var objStore = bancoHist.transaction(["historico"], "readwrite").objectStore("historico");
    var obj = objStore.add(cliente);
    obj.onsuccess = function(e) {
        console.log('Cliente finalizado!');
        // mostraHist();
    }
    obj.onerror = function(e) {
        //Materialize.toast("Houve um erro na Atualização do Banco de dados de POPs.\n Favor avisar ao programador");
        console.log("atualizabancoHist(): Houve um erro na Atualização do Banco de dados de Os's.\n Favor avisar ao programador");
        console.log("OnUpgradeNeeded: "+e.target.error);
    }
};

function deleteHist(id) {
    var objStore = bancoHist.transaction(["historico"], "readwrite").objectStore("historico");
    var obj = objStore.delete(id);
    obj.onsuccess = function () {
        console.log('histórico deletado');
        // mostraHist();
    }
}

function mostraHist() {
	var mc = [];
    var tx = bancoHist.transaction(['historico'], 'readonly');
    var store = tx.objectStore('historico');
    var index = store.index('agendaid');
    index.openCursor(null, 'prev').onsuccess = function (e) {
        var cursor = e.target.result;
	    var cor = "";
	    var btn = "";
	    var cpf = "";
        if (cursor) {
			var cv = cursor.value;
            if (cv.assinatura) {
                var ass = "1";
            } else {
                var ass = "0";
            }

            if (cv.pendente == "pendenteEnvio" ) {
                var cor = "red-text";
                var btn = '<button id="sinc" class="btn blue darken-4 waves-effect waves-light" onclick="enviaOFF(\''+cv.agendaid+'\')" ><i class="material-icons left">cached</i>Sincronizar</a>';
            }

            if (cv.agendacpf) {
                var cpf = ' CPF: '+cv.agendacpf+'<br>';
            } else {
                var cpf = '<br>';
            }

            var mostraData = cv.fim.substring(0,5);
            var reduzNome = cv.agendanome.substring(0,22)+"...";
            mc += '<li>';
            mc += '<div class="collapsible-header center '+cor+'">'+reduzNome+'<span class="right">'+mostraData+'</span></div>';
            mc += '<div class="collapsible-body"><p class="sombra" >Cliente: '+cv.agendanome+cpf;
            mc += 'Circuito: '+cv.agendacircuito+'<br>';
            mc += 'Senha: '+cv.agendasenhacirc+'<br>';
            mc += 'Contato: '+cv.agendacontato+' '+cv.agendacontatot+'<br>';
            mc += 'Endereço: '+cv.agendaendereco+'<br>';
            mc += 'Tipo: '+cv.agendatipo+'<br>';
            mc += 'Observações: '+cv.agendadesc+'<br>';
            mc += 'Finalizado: '+cv.fim+'<br>';

            if ( (cv.agendatipo.indexOf("Instalação") == -1) && (cv.agendatipo.indexOf("Ethernet") != -1) ) {
            	mc += '<h5>Detalhes RF</h5>';
            	mc += 'POP: '+cv.POP+'<br>';
            	mc += 'Distância: '+cv.distancia+'<br>';
            	mc += 'Sinal Esperado: '+cv.sinalEsperado+'<br>';
            }
            mc += btn+'<br>' ;
            mc += '</p></div>' ;
            mc += '</li>';

            cursor.continue();
    	}

        $('#colapsHist').html(mc);
        // syncing();
        onlineHist();
    }
    index.openCursor().onerror = function (e) {
        console.log("Houve um erro no Banco de dados de Históricos.\n Favor avisar ao programador");
    }
}

function onlineHist() {
    $.ajax({
        url: urlStatus,
        method: 'POST',
        timeout: 10000,
        data: {funcao: 'historico', eqpid: credenciais.eqpid},
        success: function (arg) {
            var mc = '';
            var resp = JSON.parse(arg);
            if (resp.status == 'sucesso') {
                for (var i = resp.dados.length - 1; i >= 0; i--) {
                    var cv = resp.dados[i];
                    if (cv.agendacpf) {
                        var cpf = ' <br>CPF: '+cv.agendacpf+'<br>';
                    } else {
                        var cpf = '<br>';
                    }

                    var reduzNome = cv.agendanome.substring(0,22)+"...";
                    mc += '<li>';
                    mc += '<div class="collapsible-header center ">'+reduzNome+'<span class="right">'+cv.horario+'</span></div>';
                    mc += '<div class="collapsible-body" style="text-align: left"><div class="sombra"> <p>Cliente: '+cv.agendanome+cpf;
                    if (cv.agendapac && cv.agendapac != '' && cv.agendapac != 'null' ) {
                        mc += 'Reserva: <span class="green-text">'+cv.agendapac+'</span><br>';
                    }
                    mc += 'Circuito: '+cv.agendacircuito+'<br>';
                    mc += 'Senha: '+cv.agendasenhacirc+'<br>';
                    mc += 'Contato: '+cv.agendacontato+' '+cv.agendacontatot+'<br>';
                    mc += 'Endereço: '+cv.agendaendereco+'<br>';
                    mc += 'Tipo: '+cv.agendatipo+'<br>';
                    if (cv.agendavalor && cv.agendavalor != '' && cv.agendavalor != 'null' ) {
                        mc += 'Valor Recebido: R$ <span class="blue-text"><b>'+cv.agendavalor+'</b></span><br>';
                    }

                    mc += 'Observações: '+cv.agendadesc+'</p>';
                    mc += '<h5 class="center"> Estoque </h5>';
                    if (resp.est || resp.hist) {
                        mc += '<table><thead><tr>';
                        mc += '<th> Tipo </th>';
                        mc += '<th> Item </th>';
                        mc += '<th> QTD/Pat </th>';
                        mc += '</tr></thead><tbody>';
                        if (resp.est) {
                            for (var h = resp.est.length - 1; h >= 0; h--) {
                                var r = resp.est[h];
                                if (r.agendaid == cv.agendaid) {
                                    mc += '<tr>';
                                    mc += '<td>'+r.est_tipo+'</td>';
                                    mc += '<td>'+r.item_nome+'</td>';
                                    mc += '<td>'+r.est_pat_qnt+'</td>';
                                    mc += '</tr>';
                                }
                            }
                        }
                        if (resp.hist) {
                            for (var f = resp.hist.length - 1; f >= 0; f--) {
                                var r = resp.hist[f];
                                if (r.agendaid == cv.agendaid) {
                                    mc += '<tr>';
                                    mc += '<td>'+r.hist_tipo+'</td>';
                                    mc += '<td>'+r.item_nome+'</td>';
                                    mc += '<td>'+r.hist_pat_qnt+'</td>';
                                    mc += '</tr>';
                                }
                            }
                        }
                        mc += '</tbody></table>';
                    } else {
                        mc += '<p class="center"> Sem estoque disponível </p>';
                    }
                    // mc += 'Finalizado: '+cv.fim+'<br>';
                    mc += '</div></div></li>';
                }
                $('#colapsHist').append(mc);
            } else {
                Materialize.toast('Histórico indisponível.', 3000);    
            }
        },
        error: function (argument) {
            $('#colapsHist').html('');
            Materialize.toast('Histórico indisponível.', 3000);    
        }
    })

}

// function syncing() {
//     $('.sincronizar').each(function () {
//         $(this).click(function() {
//             var internet = storage.getItem('Internet');
//             if ( internet == "ON" ) {
//                 enviaGet($(this).data('id'), $(this).data('url'), $(this).data('ass'),);
//             } else {
//                 Materialize.toast('Impossível! Sem internet.',4000);
//             }
//         });
//     });
// }

function enviaGet(id, url, ass) {
    $.get(url).success(function (resp) {
        enviado(id);
        switch (resp) {
            case "1":
                Materialize.toast("Resposta salva!", 3000);
                break;
            default:
                alert("Erro ao salvar resposta, contate o Programador!");
                break;
        }
    }).error(function () {
        alert('Erro no envio da Resposta. Tente novamente.\nID: "'+id+'" \n Url: '+url);
    });

    if (ass = "1") {
        assinatura(id);
    }
}

function assinatura(id) {
    var obj = bancoHist.transaction(["historico"], "readwrite").objectStore("historico");
    var req = obj.get(id.toString());
    req.onerror = function(event) {
        alert("Houve um erro ao buscar a info do Histórico.Favor avisar o programador.\n dbhist.js - 151");
    };
    req.onsuccess = function(event) {
        var data = event.target.result;
        $.post(urlAssinatura,{ dados: data.assinatura });
    };
}

function enviado(id) {
    var obj = bancoHist.transaction(["historico"], "readwrite").objectStore("historico");
    var req = obj.get(id.toString());
    req.onerror = function(event) {
        alert("Houve um erro ao atualizar o Histórico.Favor avisar o programador.\n dbhist.js - 104");
    };
    req.onsuccess = function(event) {
        var data = event.target.result;
        data.pendente = "Enviado: "+horaAtual();
        var requestUpdate = obj.put(data);
        requestUpdate.onerror = function(event) {
            alert("Houve um erro ao atualizar o Histórico.Favor avisar o programador.\n dbhist.js - 111");
        };
        requestUpdate.onsuccess = function(event) {
            console.log("Cliente atualizado com Sucesso!", 3000);
            // mostraHist();
        };
    };
}

