function ativa(p) {
	if (typeof(clienteAtual.ativado) == 'null') {
	    if ( clienteAtual.ativado) {
	    	Materialize.toast('Cliente já ativado!', 2000);
	    	return;
	    } else {
	    	var cl = JSON.parse(storage.getItem(clienteAtual.agendaid));
	    	if (cl.ativado) {
	    		Materialize.toast('Cliente já ativado!', 2000);
	    		return;
	    	}
	    }
	}

	if ( isNaN(Number(clienteAtual.agendapac)) && clienteAtual.agendapac.length == 7 ) {
		alert('Ativação somente para FTTN');
		return;
	}

	$('#btnAtivar').prop('disabled', true);
	var dados = {
		funcao: 'ativaPacpon',
		agid: clienteAtual.agendaid,
		nome: clienteAtual.agendanome,
		circ: clienteAtual.agendacircuito,
		eqpid: credenciais.eqpid,
		uid: credenciais.idLider,
		pac: clienteAtual.agendapac,
		lat: storage.getItem('lat'),
		lng: storage.getItem('lng')
	}
	
	$.ajax({
		url: urlAtiva,
		method: 'post',
		data: dados,
		success: (res) => {
			$('#btnAtivar').prop('disabled', false);
			console.log(res);
			var resp = JSON.parse(res);
			alert(resp.msg);
			if (resp.status == 'sucesso') {
				if (storage.getItem(clienteAtual.agendaid)) {
		            clienteAtual = JSON.parse(storage.getItem(clienteAtual.agendaid));
		            clienteAtual.ativado = true;
		            storage.setItem(clienteAtual.agendaid, JSON.stringify(clienteAtual));
				} else {
		            clienteAtual.ativado = true;
				}
			}
		},
		error: (err) => {
			console.log(err)
			$('#btnAtivar').prop('disabled', false);
		}
	})
}