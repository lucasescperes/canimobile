
urlChat = 'http://canicontrol.vetorial.net/class/ChatDao.php'

function atualizaChat () {
    console.log('updt');
  $.post(urlChat, {
    funcao: 'chatAppUpdt',
    equipeID: equipeID,
  }).done(function (argument) {
    $('#msgBody').html(argument);
    $("#msgBody").scrollTop($("#msgBody")[0].scrollHeight);
  }).fail(function(argument) {
    alert("Favor, mande um print desse erro para o Programador:\n"+argument);
  });
}

function comecaChat(){
  chatComecou = setInterval(atualizaChat, 1000); 
}
function terminaChat(){
  clearInterval(chatComecou);
}

$('#msgSend').click(function () {
  whatssApp();
});

function whatssApp(){
  var texto = $('#msgText').val(); 
  $.ajax({
    url: urlChat,
    method: 'post',
    data: {
      funcao: 'chatAppSend',
      equipeID: equipeID,
      texto: texto,
    }
  }).fail(function(argument) {
      alert("Favor, mande um print desse erro para o Programador:\n"+argument);
  });
  $('#msgText').val(''); 
}