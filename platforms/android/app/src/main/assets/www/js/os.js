$('#btnMove').click(function () {
    if ( clienteAtual.agendaid == "0" || typeof(clienteAtual.agendaid) == 'undefined') {
        Materialize.toast("Selecione 1 cliente.",3000);
        return;
    }
    
    if ( storage.getItem(clienteAtual.agendaid) ) {
        clienteAtual = JSON.parse(storage.getItem(clienteAtual.agendaid));
    }

    $('#btnMove').prepend('<i id="load" class="fa fa-spinner fa-spin left"></i>');
    // Testa se o usuário já escolheu 1 OS.

    $('#valForm').html('');
    $('#atualizaOs').addClass('hide');
    $('#btnMove').addClass('hide');
    $('#pause').removeClass('hide');
    $('#btnEncerra').addClass('hide');
    $('.btnconfirma').addClass('desaparece');
    $('#btnConsulta').addClass('desaparece');
    $('.div-confirma').removeClass('desaparece');
    $('#btnInicio').removeClass('hide');
    $('#load').remove();

    switch (status) {
        case "Em ligação com o NOC.":
        case "Trabalhando.":
            Materialize.toast("Continuando - Trabalhando", 4000);
            $('#btnInicio').addClass('hide');
            tipodeS(clienteAtual.agendatipo);
            break;
        case "Em deslocamento.":
            Materialize.toast("Continuando - Em deslocamento.", 4000);
            mostraMapa();
            break;
        default:
            clienteAtual.deslocamento = horaAtual();
            status = "Em deslocamento.";
            clienteAtual.agendastatus = "Em deslocamento.";
            //ATUALIZA O ESTADO DESSE AGENDAMENTO PARA "Em Deslocamento"

            atualizaStatus("Em deslocamento.");
            mostraMapa();

            // Guarda hora do deslocamento
            storage.setItem(clienteAtual.agendaid, JSON.stringify(clienteAtual));
            break;
    }
});

function atualizaStatus(agendastatus, desc) {
    var dados = {
            funcao: "novoStatus", 
            agendaid: clienteAtual.agendaid,
            status: agendastatus,
            eqpid: credenciais.eqpid,
            lat: storage.getItem('lat'),
            lng: storage.getItem('lng'),
        };
    if (agendastatus == 'Em Pausa.') {
        dados.desc = desc;
    }
    $.ajax({
        method: "POST",
        url: urlStatus,
        data: dados,
        success: function(arg){
            var resp = JSON.parse(arg);
            if (resp.status == 'sucesso') {
                Materialize.toast(resp.msg, 3000);
            } else {
                alert(resp.msg);
                alert('O App continuará OFFLine');
            }
        },
        error: function (erro) {
            Materialize.toast("Off: Status atualizado. " +agendastatus, 3000);
        }
    });
}

$('#btnInicio').click(function () {
    status = "Trabalhando.";
    if ( storage.getItem('accu') > 40) {
        // alert('Melhorando Coordenadas. Aguarde.');
        var conf = window.confirm('Precisão das coordenadas baixa. Posso tentar melhorar?');
        if (conf) {
            Materialize.toast('Obrigada!', 2000);
            $('#btnUpdt').click();
            return ;
        }
    }
    $('#btnInicio').prepend('<i id="load" class="fa fa-spinner fa-spin left"></i>');
    if ( (lat < 0) && (lng < 0) ) {
        $('#map').addClass('hide');
        $('#hideMap').addClass('hide');
    } else  {
        lat = storage.getItem('lat');
        lng = storage.getItem('lng');
    }

 	//ATUALIZA O ESTADO DESSE AGENDAMENTO PARA "Iniciando Serviço"
    atualizaStatus("Trabalhando.");

    // Esse bloco previne que, caso o STATUS pule diretamente pra cá,
    // procure por um getItem não existente (acredito que só aconteceu enquanto eu estava testando)
    if ( storage.getItem(clienteAtual.agendaid) ) {
        clienteAtual = JSON.parse(storage.getItem(clienteAtual.agendaid));
        clienteAtual.trabalhando = horaAtual();
        clienteAtual.agendastatus = "Trabalhando.";
        storage.setItem(clienteAtual.agendaid, JSON.stringify(clienteAtual));
    } else {
        storage.setItem(clienteAtual.agendaid, JSON.stringify(clienteAtual));
    }

    $('#load').remove();
    $('#btnInicio').addClass('hide');
    tipodeS(clienteAtual.agendatipo);
});

// Define se mostra os setores ou o Botão de Ativação FTTN

function tipodeS (qual) {
    if ( qual.indexOf("Cond") == -1 && qual.indexOf("MAX") == -1 && (qual.indexOf("Fibra") != -1 || qual.indexOf("Manutenção") != -1 || qual.indexOf("Ethernet") != -1 || qual.indexOf("Transferência") != -1 || qual.indexOf("Ativação") != -1 )) {
        $("#btnAtiva").removeClass("hide");
    }
    if (qual.indexOf("Fibra") == -1 && qual.indexOf("Ethernet") == -1 && (qual.indexOf("Instalação") != -1 || qual.indexOf("Temporada") != -1 || qual.indexOf("Manutenção") != -1) ) {
        retSet();
    }

    if (qual) {
        sinalEsperado = qual;
    } else {
        sinalEsperado = " - ";
    }
    distancia = " - ";
    pop = " - ";

    $('.btnconfirma').removeClass("desaparece");
    $('#btngps').addClass("hide");
    $('#btnEncerra').removeClass("hide");
    $('#btnEstoque').removeClass("hide");
    if (clienteAtual.agendatipo == "Viabilidade") {
        $('#bntFormCorp').removeClass("hide");
    }
}
