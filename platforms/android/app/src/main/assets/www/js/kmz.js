$('#btnMapNav').click(()=>{
  $("#mapNav").css('height',"100%");
  initMapa();
})

$('#btnFechaMapa').click(()=>{
  $("#mapNav").css('height',"0%");
})

function initMapa() {
	var posicao = {lat: Number(storage.getItem('lat')), lng: Number(storage.getItem('lng'))};
	var tamanho = $('#mapNav').height();
	var tamanho = tamanho * 90 / 100;
	$('#mapa').height(tamanho);

    mapa = new google.maps.Map(document.getElementById('mapa'), {
    center: posicao,
    zoomControl: true,
    mapTypeControl: false,
    scaleControl: true,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: true,
    styles: [
          {
            featureType: 'poi',
            stylers: [{visibility: 'off'}]
          },
          {
            featureType: 'transit',
            elementType: 'labels.icon',
            stylers: [{visibility: 'on'}]
          },{
            featureType: "landscape",
            elementType: "labels",
            stylers: [
              { "visibility": "on" }
            ]
          }
        ],
    gestureHandling: 'greedy'
    });
  	
 	
 	infoW = new google.maps.InfoWindow;
    var marker = new google.maps.Marker({
        position: posicao,
        map: mapa
    });

    mapa.setZoom(14);
    mapa.panTo(marker.position);

    $.ajax({
    	url: 'https://canicontrol.vetorial.net/api/app/rf.php',
    	method: 'post',
    	data: {funcao: 'listaPOPDB', regiao:''},
    	success: (arg)=>{
    		var resp = JSON.parse(arg);
    		kmz(resp);
    	},
    	error: ()=>{
    		alert('Erro ao carregar mapa, verifique sua conexão com a internet');
    	}
    })
};

function kmz(poly) {
	console.log(poly);
	for (var i = poly.length - 1; i >= 0; i--) {
		var pol = poly[i];
		console.log(pol);
		var coords = {lat: Number(pol.poplat),lng: Number(pol.poplng)};
		var posit = {lat: Number(storage.getItem('lat')), lng: Number(storage.getItem('lng'))};
		new google.maps.Marker({
	        position: coords,
	        map: mapa,
	        icon: 'https://maps.google.com/mapfiles/kml/shapes/parking_lot_maps.png',
	        cNome:pol.popnome
	    }).addListener('click', function (event) {
	    	console.log(event);
	    	console.log(this);
	    	var posit = {lat: Number(storage.getItem('lat')), lng: Number(storage.getItem('lng'))};
	    	var coords = {lat:Number(this.position.lat()),lng:Number(this.position.lng())};
	    	var dis = dist(posit, coords);
			infoW.close();
			infoW.setContent(this.cNome+'<br>'+dis);
			infoW.setPosition(event.latLng);
			infoW.open(mapa);	
	    });
	}
}
