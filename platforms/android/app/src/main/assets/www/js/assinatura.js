var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
	backgroundColor: 'rgba(255, 255, 255, 0)',
	penColor: 'rgb(0, 0, 0)'
});

$('#clear').click(function (event) {
	signaturePad.clear();
});

$('#assinante').click(function () {

	var nome = clienteAtual.agendanome;
	var nomeAssinante = $.trim($('#recebenome').val());
	storage.setItem('nomeAssinante', nomeAssinante);

	var nomesemespaco = $.trim(nome).split(' ')[0];
	for (var i = 1; i < $.trim(nome).split(' ').length; i++) {
		var nomesemespaco = nomesemespaco+"_"+$.trim(nome).split(' ')[i];
	}
	storage.setItem('nomesemespaco', nomesemespaco);
	$('#corpoQuest').addClass('hide');
	$('#corpoMAssinatura').removeClass('hide');
	
});

$('#save').click(function () {
	if (signaturePad.isEmpty()) {
		Materialize.toast('Favor, preencher assinatura.',4000);
		return;
	}
	storage.setItem('assinatura', signaturePad.toDataURL());
	$('#corpoConfirmaImagem').html('<h5>'+storage.getItem('nomeAssinante')+'</h5>');
	$('#corpoConfirmaImagem').append('<img src="'+storage.getItem('assinatura')+'"/ width="400" height="200">');
	$('#corpoMAssinatura').addClass('hide');
	$('#corpoConfirma').removeClass('hide');
});

$('#assConfirmado').click(function () {
	$('#assConfirmado').prepend('<i id="load" class="fa fa-spinner fa-spin left"></i>');
    var dados = {
            funcao: "Assinatura",
            agendaid: clienteAtual.agendaid,
            assinatura: storage.getItem('assinatura'),
            nome: storage.getItem('nomesemespaco'),
            nomeAssinante: storage.getItem('nomeAssinante')
    };

    $.post(urlAssinatura,{
        dados: dados
    });

    if ( storage.getItem('Internet') == "OFF" ) {
    	clienteAtual = JSON.parse(storage.getItem(clienteAtual.agendaid));
        clienteAtual.assinatura = dados;
        storage.setItem(clienteAtual.agendaid, JSON.stringify(clienteAtual));
    };

	$('#load').remove();
	signaturePad.clear();
	$('#corpoConfirmaImagem').html('');
	$('#corpoMAssinatura').addClass('hide');
	$('#corpoConfirma').addClass('hide');
	$('#recebenome').val('');
	$('#corpoDevolver').removeClass('hide');
});



$('#assVoltar').click(function () {
	signaturePad.clear();
	$('#corpoConfirma').addClass('hide');
	$('#corpoQuest').removeClass('hide');
});

$('#retornaAss').click(function (event) {
	event.preventDefault();
	openNav();
	event.stopPropagation();
});


function openNav() {
    document.getElementById("myNav").style.height = "100%";
}

function closeNav() {
    document.getElementById("myNav").style.height = "0%";
    signaturePad.clear();
	$('#corpoConfirmaImagem').html('');
	$('#corpoMAssinatura').addClass('hide');
	$('#corpoConfirma').addClass('hide');
	$('#corpoDevolver').addClass('hide');
	$('#corpoPesQual').addClass('hide');
	$('#corpoQuest').removeClass('hide');
	$('#recebenome').val('');
}

