clienteAtual = [];
//A cada nova execução do APP a lista de serviço é montada novamente!
indexedDB.deleteDatabase('listaServicos');

function chamaDBos() {

    var reqOs = window.indexedDB.open("listaServicos", storage.getItem("versaoDBos"));

    reqOs.onupgradeneeded = function(e) {
        var dbo = e.target.result;
        if ( !dbo.objectStoreNames.contains('servicos') ) {
            var oS = dbo.createObjectStore("servicos", { keyPath: "id", autoIncrement: true });
            oS.createIndex("id", "id", { unique: false });
            oS.createIndex("agendaid", "agendaid", { unique: true });
            oS.createIndex("data", "data", { unique: false });
            oS.createIndex("agendanome", "agendanome", { unique: false });
            oS.createIndex("agendapac", "agendapac", { unique: false });
            oS.createIndex("agendaendereco", "agendaendereco", { unique: false });
            oS.createIndex("agendatipo", "agendatipo", { unique: false });
            oS.createIndex("agendacircuito", "agendacircuito", { unique: false });
            oS.createIndex("agendapop", "agendapop", { unique: false });
            oS.createIndex("agendasinal", "agendasinal", { unique: false });
            oS.createIndex("agendastatus", "agendastatus", { unique: false });
            oS.createIndex("agendafim", "agendafim", { unique: false });
            oS.createIndex("agendavalor", "agendavalor", { unique: false });
            oS.createIndex("agendaformapag", "agendaformapag", { unique: false });
        }
    }

    reqOs.onerror = function(e) {
      alert('Erro ao abrir o Banco de dados de Ordens de Serviço: Linha 29 dbos.js\n Avisar ao programador.');
    };

    reqOs.onsuccess = function(e) {
        console.log('Abrindo o DB de OSs');
        bancoOS = e.target.result;
    };    
}

function atualizaLista() {
    $('#btnMove').prop('disabled', true);
    clienteAtual = [];
    listadeOS = "";
    $('#screen').html('');
    $('#valForm').html('');
    $('#detail').html('');
    $('#informacao').html('');

    $.ajax({
        timeout: 5000,
        async: false,
        method: "POST",
        url: urlOs,
        datatype: "html",
        data: {funcao: "listaDBOS", eqpid: credenciais.eqpid},
        success: function(os){
            var resp = JSON.parse(os);
            console.log(resp);
            console.log(resp.os);
            listadeOS = resp.os;
            if (resp.status == 'erro') {
                $('#btnMove').addClass('hide');
                $('#detail').html('<div class="center green-text darken-1" style="font-size:2rem;margin-top:50px;"> Agenda Livre.</div>');
                $('#loader-wrapper').fadeOut( "slow" );
                $('#loader').fadeOut( "slow" );
                return;
    		}
        	console.log('lista de Os\'s atualizada');
            $('#btnMove').removeClass('hide');
            limpaOs(listadeOS);
            $('#loader-wrapper').fadeOut( "slow" );
            $('#loader').fadeOut( "slow" );
        },
        error: function(erro) {
            Materialize.toast('Usando lista Offline', 4000);
            mostraOs();
            $('#loader-wrapper').fadeOut( "slow" );
            $('#loader').fadeOut( "slow" );
            console.log(erro);
        }
    });
    $("#listaos").removeClass("hide");
    $(".div-confirma").removeClass("hide");

};

// Quando tem internet ele baixa a limpa a lista, depois atualiza o DB
function limpaOs(listadeOS) {
    objStore = bancoOS.transaction(["servicos"], "readwrite").objectStore("servicos");
    limpa = objStore.clear().onsuccess = insereOsServer(listadeOS);
    if (typeof(limpa) == 'undefined') {
        limpa = objStore.clear().onerror = insereOsServer(listadeOS);
    }
}

// enviamsgerro = true;

// function msgerro(msg) {
//     setTimeout(function() {
//         if (msg == 'QuotaExceededError') {
//             alert('Sem espaço em disco para mostrar OS\'s');
//         } else {
//             alert(msg);
//         }
//         enviamsgerro = true;
//     }, 2000);
// }
// Atualiza a Lista de OSs do Banco de dados

function insereOsServer(listadeOS) {
    objStore2 = bancoOS.transaction(["servicos"], "readwrite").objectStore("servicos");
    for (var i = 0; i < listadeOS.length; i++) {
        var obj = objStore2.put(listadeOS[i]);
        obj.onsuccess = function(e) {
            // console.log('OnUpgradeNeeded: OS Adicionado ao Banco de dados');
            console.log(e);    
        }
        obj.onerror = function(e) {
            console.log("Houve um erro na Atualização do Banco de dados de Os's.\n Favor avisar ao programador");
            console.log("OnUpgradeNeeded: "+e.target.error);
        }

    };
    mostraOs();
}

function atualizaListaOsUnica(id) {
    var obj = bancoOS.transaction(["servicos"], "readwrite").objectStore("servicos");
    var req = obj.get(Number(id));
    req.onerror = function(event) {
        alert("Houve um erro ao aadicionar o status Concluído na OS " + id);
        atualizaLista();
    };
    req.onsuccess = function(event) {
        var data = event.target.result;
        debug = data;
        data.agendastatus = "Concluído.";
        var requestUpdate = obj.put(data);
        requestUpdate.onerror = function(event) {
            alert("Houve um erro ao aadicionar o status Concluído na OS " + id);
            atualizaLista();
        };
        requestUpdate.onsuccess = function(event) {
            console.log("OS atualizado com Sucesso!", 3000);
            atualizaLista();
        };
    };
}

function mostraOs() {
    // reseta parametros
    sinalEsperado = "";
    distancia = "";
    pop = "";
    
    var mc = [];
    // usado par afazer com que o botão RADIUS funcione direito
    var i = 0;
    var tamanhoCursor = 0;
    var tx = bancoOS.transaction(['servicos'], 'readonly');
    var store = tx.objectStore('servicos');
    var index = store.index('id');
    index.openCursor(null, 'next').onsuccess = function (e) {
        var cursor = e.target.result;
        if (cursor) {
            console.log(cursor.value.agendaid);
            console.log(cursor.value.id);
        	if ( cursor.value.agendastatus != "Concluído.") {
        		tamanhoCursor++;
        		var cv = cursor.value;
                if ( (cv.agendalat < 0) && (cv.agendalng < 0) ) {
                    var cliente = new google.maps.LatLng(cv.agendalat, cv.agendalng);
                    var equipe = new google.maps.LatLng(storage.getItem('lat') , storage.getItem('lng'));
                    var distancia = google.maps.geometry.spherical.computeDistanceBetween(equipe, cliente);
                    var opt = Math.floor(distancia) + "m ";
                } else {
                    var opt = 'N/A';
                }
                console.log(opt);
                if (cv.data < 13) {
                    var cor = 'green-text';
                } else {
                    var cor = 'blue-text';
                }

                if (cv.data == 7 || cv.data == 13) {
                    var cor = 'red-text';
                }

                var nomeCpl = cv.agendanome.capitalize();
	            mc += '<p><input class="valorOS" name="group1" type="radio" id="test'+i+'" value="'+cv.agendaid+'" data-id="'+cv.id+'" data-valor="'+cv.agendavalor+'" data-formapag="'+cv.agendaformapag+'"' ;
	            mc += '" data-banda="'+cv.agendabanda+'" data-nome="'+nomeCpl+'" data-end="'+cv.agendaendereco+'" data-data="'+cv.data+'" data-lat="'+cv.agendalat ;
	            mc += '" data-pac="'+cv.agendapac+'" data-lng="'+cv.agendalng+'" data-tipo="'+cv.agendatipo+'" data-status="'+cv.agendastatus+'" data-obs="'+cv.agendadesc ;
	            mc += '" data-contato="'+cv.agendacontato+'" data-contatot="'+cv.agendacontatot+'" data-circ="'+cv.agendacircuito+'" data-senhacirc="'+cv.agendasenhacirc+'" "/>';
	            mc += '<label class="'+cor+'" for="test'+i+'"><span id="dist" class="left">'+opt+'</span>';
	            mc += ' &nbsp;'+nomeCpl+'</label></p>';
	        	i++;
	            cursor.continue();
        	}
        }

        if (tamanhoCursor > 0) {
            $('#btnMove').removeClass('hide');
	        $('#valForm').html(mc); 
            $('#loader-wrapper').fadeOut( "slow" );
            $('#loader').fadeOut( "slow" );
            iniciaOs();
        } else {
			$('#btnMove').addClass('hide');
			$('#detail').html('<div class="center green-text darken-1" style="font-size:2rem;margin-top:50px;"> Agenda Livre.</div>');
			$('#loader-wrapper').fadeOut( "slow" );
			$('#loader').fadeOut( "slow" );
        }
    }
    index.openCursor().onerror = function (e) {
        console.log("Houve um erro no Banco de dados de OSs.\n Favor avisar ao programador");
        $('#btnMove').addClass('hide');
        $('#detail').html('<div class="center green-text darken-1" style="font-size:2rem;margin-top:50px;"> Agenda Livre.</div>');
        $('#loader-wrapper').fadeOut( "slow" );
        $('#loader').fadeOut( "slow" );
    }
}
