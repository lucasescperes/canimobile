masterPic = "";
$("#cameraPequena").click(function () {
   if (masterPic != "") {
         Materialize.toast('Aguarde completar o envio!',3000);
         return;
   }
   
   if (clienteAtual == "" || clienteAtual == null ) {
      alert("Favor escolher uma OS primeiro");
      return;
   }

   if (storage.getItem('internet') == "OFF" || checkConnection() == 'Sem Internet') {
      alert("Certifique-se de ter internet para enviar fotos");
      return; 
   }

   if (checkConnection() != 'Conexão WiFi' ) {
      var conf = confirm("Deseja enviar a foto usando "+ checkConnection());
      if (!conf) {
         return;
      }
   }

   navigator.camera.getPicture(sucessoFoto, falhaFoto, {  
      quality: 80, 
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: false,
      encodingType: Camera.EncodingType.JPEG,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      destinationType: Camera.DestinationType.DATA_URL
   });
});

function sucessoFoto(imageData) { 
   masterPic = imageData;
   $('#modalDescPic').modal('open');
}

function falhaFoto(message) {
   masterPic = "";
   //alert('Favor tentar novamente\nInformar erro ao desenvolvedor: "' + message + "\""); 
} 

$('#mandaNudes').click(function (e) {
   e.preventDefault();
   $('#modalDescPic').modal('close');
   var desc = "";

   var desc = $('#descPic').val();

   // if (desc == "") {
   //    Materialize.toast("Preencha uma descrição para a foto", 3000);
   //    return;
   // }

   var nome = clienteAtual.agendanome;
   var nomesemespaco = $.trim(nome).split(' ')[0];
   for (var i = 1; i < $.trim(nome).split(' ').length; i++) {
      var nomesemespaco = nomesemespaco+"_"+$.trim(nome).split(' ')[i];
   }

   var dados = {
      funcao: "Fotos",
      nome: nomesemespaco,
      agendaid: clienteAtual.agendaid,
      imgdata: masterPic,
      descricao: desc
   }
   
   $('#mandaNudes').html('<i id="load" class="fa fa-spinner fa-spin left"></i> Enviando');
   var percent = $('.percent');
   var percentComplete = '0%';
   percent.html('0%');

$.ajax({
   xhr: function() {
      enviando = true;
      var xhr = new window.XMLHttpRequest();

      xhr.upload.addEventListener("progress", function(evt) {
         $('.percent').removeClass('hide');
         if (evt.lengthComputable) {
            var percentComplete = evt.loaded / evt.total;
            percentComplete = Math.ceil(parseInt(percentComplete * 100));
            percent.html(percentComplete.toString() + '%');

            if (percentComplete === 100) {
               percent.html('<i class="material-icons green-text lighten-2">done_all</i>');

            }
         }
   }, false);

    return xhr;
  },
  url: urlFotos,
  type: "POST",
  data: dados,
  success: function(arg) {
      $('#mandaNudes').html('Salvar');
      $('.percent').addClass('hide');
      $('.percent').html('0%');
      $('#modalDescPic').modal('close');
      console.log(arg);
      if (arg == "1") {
         Materialize.toast("Foto Salva!", 3000);
      }
      masterPic = "";
      $('#descPic').val('');
  },
  error: function (argument) {
      $('.percent').addClass('hide');
      $('.percent').html('0%');
      masterPic = "";
      Materialize.toast("Erro ao enviar Foto!", 3000);
  }
});


   // $.post(urlFotos,dados).done(function (arg) {
   //    $('#mandaNudes').html('Salvar');
   //    $('#modalDescPic').modal('close');
   //    console.log(arg);
   //    if (arg == "1") {
   //       Materialize.toast("Foto Salva!", 3000);
   //    }
   //    masterPic = "";
   //    $('#descPic').val('');
   // });

   //  $.ajax({
   //      type:'POST',
   //      url: urlFotos,
   //      data: dados,
   //      xhr: function() {
   //              var myXhr = $.ajaxSettings.xhr();
   //              if(myXhr.upload){
   //                  myXhr.upload.addEventListener('progress',progress, false);
   //              }
   //              return myXhr;
   //      },
   //      // cache:false,
   //      // contentType: false,
   //      // processData: false,

   //      success:function(data){
   //          console.log(data);

   //        alert('data returned successfully');

   //      },

   //      error: function(data){
   //          console.log(data);
   //      }
   //  });

   // function progress(e){

   //    if(e.lengthComputable){
   //       var max = e.total;
   //       var current = e.loaded;

   //       var Percentage = (current * 100)/max;
   //       console.log(Percentage);
   //       if(Percentage >= 100){
   //         // process completed  
   //       }
   //    }
   // }  
    


   
});


// function getFileEntry(imageData) {
//     window.resolveLocalFileSystemURL(imageData, function success(fileEntry) {

//         // Do something with the FileEntry object, like write to it, upload it, etc.
//         // writeFile(fileEntry, imgUri);
//         console.log("got file: " + fileEntry.fullPath);
//         // displayFileData(fileEntry.nativeURL, "Native URL");

//     }, function () {
//       // If don't get the FileEntry (which may happen when testing
//       // on some emulators), copy to a new FileEntry.
//         createNewFileEntry(imageData);
//     });
// }






// function movePic(file){ 
//     window.resolveLocalFileSystemURI(file, resolveOnSuccess, resOnError); 
// } 

// //Callback function when the file system uri has been resolved
// function resolveOnSuccess(entry){ 
//     var d = new Date();
//     var n = d.getTime();
//     //new file name
//     var newFileName = n + ".jpg";
//     var myFolderApp = "EasyPacking";

//     window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSys) {      
//     //The folder is created if doesn't exist
//     fileSys.root.getDirectory( myFolderApp,
//                     {create:true, exclusive: false},
//                     function(directory) {
//                         entry.moveTo(directory, newFileName,  successMove, resOnError);
//                     },
//                     resOnError);
//                     },
//     resOnError);
// }

// //Callback function when the file has been moved successfully - inserting the complete path
// function successMove(entry) {
//     //I do my insert with "entry.fullPath" as for the path
// }

// function resOnError(error) {
//     alert(error.code);
// }

